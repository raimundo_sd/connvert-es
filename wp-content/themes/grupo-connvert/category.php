<?php get_header(); ?>
    <section class="list-posts-blog">
        <div class="container">
            <div class="row">
				<?php while(have_posts()):the_post(); ?>
					<?php get_template_part("template-parts/card-blog"); ?>
				<?php endwhile; ?>
            </div>
            <div class="wrap-pagination">
                <div class="pagination">
					<?php
					if(function_exists('wp_pagenavi')) {
						wp_pagenavi();
					}else{
						echo paginate_links();
					}
					?>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>