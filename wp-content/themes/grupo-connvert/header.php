<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package solucaodigital
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <?php // coloca cor no topo do mobile ?>
    <meta name="theme-color" content="#16151C">
    <meta name="msapplication-navbutton-color" content="#16151C">
    <meta name="apple-mobile-web-app-status-bar-style" content="#16151C">
    <meta name="format-detection" content="telephone=no">

    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600;700;800&display=swap" rel="stylesheet" media="none" onload="this.media='all'">
    <link rel="preload" href="/wp-content/uploads/2021/03/homepage-banner.jpg.webp" as="image" />
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="wrap-page">
        <header>
        <div class="container">
            <a href="<?php echo site_url(); ?>" class="logo" title="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                <img src="<?php echo get_theme_mod( 'project_logo' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
            </a>
            <a title="Menu" href="#" class="open-menu"><i class="fa fa-bars"></i></a>
            <nav>
                <?php wp_nav_menu(array("menu" => "principal", "container" => false)); ?>
            </nav>

        </div>
    </header>