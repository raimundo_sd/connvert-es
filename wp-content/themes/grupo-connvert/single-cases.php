<?php get_header(); ?>
<?php the_post(); ?>

    <section class="content-single-post content-single-case">

        <div class="container sec-dark title-header">
             <div class="box-text">
                 <h1 class="title-large"><?php the_title(); ?></h1>
                 <h2 class="subtitle"><?php echo  get_post_meta(get_the_ID(), 'subtitulo', true) ?></h2>
             </div>
            <div>
                <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'large'); ?>" alt="">
            </div>

        </div>

        <div class="container inner-post">
            <div class="breadcrumbs">
                <ul>
                </ul>
            </div>

            <div class="content-text">
                <?php the_content(); ?>
            </div>

    </section>






<?php get_footer(); ?>
