<div class="card-executivos">
    <a title="Ver executivo" href="<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail_url('medium'); ?>" alt=""> </a>

        <a href="<?php the_permalink(); ?>"><p class="title"><?php the_title(); ?></p></a>
        <span class="cargo"><?php echo get_post_meta( get_the_ID(), 'cargo', true ); ?></span>

   <a title="Veja mais" href="<?php the_permalink(); ?>" class="bt bt-vazado">Ver más</a>
</div>
