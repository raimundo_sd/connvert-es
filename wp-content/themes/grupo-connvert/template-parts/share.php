<div class="bt-compartilhar">
	<?php $urlencoded_titulo = str_replace(' ', '+', get_the_title()); ?>
	<a class="btn-share" role="button" href="javascript:void(0);" onclick="popUp('http://www.facebook.com/sharer.php?u=<?php echo "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>')" target="_blank"><i class="fab fa-facebook-square"></i></a>
	<a class="btn-share" role="button" href="javascript:void(0);" onclick="popUp('http://twitter.com/share?url=<?php echo "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>&text=<?php echo $urlencoded_titulo;?>')" target="_blank"><i class="fab fa-twitter-square"></i></a>
    <a class="btn-share" role="button" href="javascript:void(0);" onclick="popUp('http://www.linkedin.com/shareArticle?mini=true&url=<?php echo "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>')" target="_blank"><i class="fab fa-linkedin"></i></a>
    <a class="btn-share" role="button" href="https://api.whatsapp.com/send?text=Veja: <?php echo get_the_title(). " em http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>" target="_blank"><i class="fab fa-whatsapp-square"></i></a>
</div>