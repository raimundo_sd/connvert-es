<div class="card-case">
    <div class="content-thumb">
        <a class="thumb lazyload" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
            <img alt="<?php the_title(); ?>" src="<?php the_post_thumbnail_url('small'); ?>">
        </a>
    </div>
    <a href="<?php the_permalink(); ?>">
        <h3><?php the_title(); ?></h3>
    </a>
    <div class="content-text">
        <?php the_excerpt();?>
    </div>
    <a href="<?php the_permalink(); ?>" class="bt bt-vazado">Ver más</a>
</div>
