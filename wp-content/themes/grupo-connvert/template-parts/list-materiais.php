
<div class="list-materiais">
    <div class="listar-materiais">
        <div class="bt-mobile bt-prev1"><i class="fas fa-chevron-left"></i></div>
        <ul class=" js-tabmenu">
            <?php
            $materiais = array();
            $categorias = get_terms( 'categoria_materiais', array( 'parent' => 0 ));
            foreach ($categorias as $categoria){
                $query = new WP_Query(array(
                    'post_type' => 'materiais',
                    'tax_query' => array(
                        array(
                            'field' => 'slug',
                            'terms' => $categoria->slug,
                            'taxonomy' => 'categoria_materiais',
                        ),
                    ),
                ));
                if($query->have_posts()) {
                    while ( $query->have_posts() ) {
                        $query->the_post();
                        $subcategoria = 0;
                        $subCat = wp_get_post_terms(get_the_ID(), 'categoria_materiais');
                        foreach ($subCat as $_term) {
                            if ($_term->parent != 0)
                                $subcategoria = $_term->name;
                        }
                        $materiais[$categoria->slug][$subcategoria][] = array('ID' => get_the_ID(), 'titulo' => get_the_title());
                    }
                }
                echo '<li categoria="'.$categoria->slug.'">'.$categoria->name.'</li>';
            }
            wp_reset_query();
            ?>
        </ul>
        <div class="bt-mobile bt-next1"><i class="fas fa-chevron-right"></i></div>

    </div>

    <div class="js-tabcontent">
        <?php
        $subcategoriaAtual = $subcategoriaNova = "";
        foreach ($materiais as $categoriaKey => $materialCategoria){
            echo '<section categoria="'.$categoriaKey.'">';
            foreach ($materialCategoria as $subcategoriaNova => $materialSub){
	            if($subcategoriaAtual != $subcategoriaNova)
		            $subcategoriaAtual = $subcategoriaNova;
	            if($subcategoriaNova != 0){
                    echo '<p>'.$subcategoriaNova.'</p>';
                }

                foreach ($materialSub as $material){ ?>
                    <?php echo get_the_title($material['ID']); ?>  <a target="_blank" href="<?php echo wp_get_attachment_url(get_post_meta($material['ID'], 'material', true)); ?>"> <img src="<?php echo get_template_directory_uri(); ?>/images/ico-download.png" alt=""></a>
                <?php
                }
            }
            echo '</section>';
        }
        ?>
    </div>
</div>







