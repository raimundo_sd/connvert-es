
<div class="list-administrativos">
        <?php
        $query_adm = new WP_Query(array(
            'post_type' => 'administrativo',
        ));

        if($query_adm->have_posts()){
            while ($query_adm->have_posts()){
                $query_adm->the_post(); ?>
                <div class="wrapper">
                    <div><?php the_title(); ?></div>
                    <div><a target="_blank" href="<?php echo wp_get_attachment_url(get_post_meta(get_the_ID(), 'administrativo', true)); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/ico-download.png" alt=""></a></div>
                </div >
            <?php }
        }

        ?>
</div>



