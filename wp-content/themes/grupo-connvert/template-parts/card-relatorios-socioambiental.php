<div class="card-socioambiental">
    <a target="_blank" href="<?php echo wp_get_attachment_url(get_post_meta(get_the_ID(), 'socioambiental', true)); ?>"><?php the_title(); ?></a>
</div>
