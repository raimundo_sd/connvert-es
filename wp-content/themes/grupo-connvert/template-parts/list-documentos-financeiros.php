
<?php

$financeiro = $financeiro2 = array();
$term_tri = array();
$id_tri = array();

$custom_terms = get_terms('trimestre_financeiro');
foreach ($custom_terms as $terms_trimestre){
    $term_tri[] = $terms_trimestre->term_id;
}
$args = array('post_type' => 'financeiro', 'post_status'  => 'publish',
    'posts_per_page' => -1,
    'tax_query' => array(
        array(
            'taxonomy' => 'trimestre_financeiro',
            'field' => 'term_id',
            'terms' => $term_tri
        ),
    ),
);
$query = new WP_Query( $args );

if($query->have_posts()){
    while ($query->have_posts()){
        $query->the_post();
        $ano = wp_get_post_terms(get_the_ID(), 'anos_financeiro');
        $categoria = wp_get_post_terms(get_the_ID(), 'categoria_financeiro');
        $trimestre = 0;
        if($trim = wp_get_post_terms(get_the_ID(), 'trimestre_financeiro'))
            $trimestre = $trim[0]->name;
        $financeiro[$ano[0]->name][$categoria[0]->name][$trimestre][] = get_the_ID();

    } wp_reset_query();
}

$args_ano = array('post_type' => 'financeiro', 'post_status'  => 'publish',
    'posts_per_page' => -1,
    'tax_query' => array(
        array(
            'taxonomy' => 'trimestre_financeiro',
            'field' => 'term_id',
            'terms' => $term_tri,
            'operator' => 'NOT IN'
        ),
    ),
);

$query_ano = new WP_Query( $args_ano );

if($query_ano->have_posts()){
    while ($query_ano->have_posts()){
        $query_ano->the_post();
        $ano2 = wp_get_post_terms(get_the_ID(), 'anos_financeiro');
        $categoria2 = wp_get_post_terms(get_the_ID(), 'categoria_financeiro');
        $financeiro2[$ano2[0]->name][$categoria2[0]->name][] = get_the_ID();
    } wp_reset_query();
}
?>

<div class="list-documentos">
    <div class="listar-anos">
        <div class="bt-mobile bt-prev"><i class="fas fa-chevron-left"></i></div>
        <ul class="list-title">
            <?php $anos = get_terms(array(
                    'taxonomy' => 'anos_financeiro',
                    'orderby' => 'name',
                    'order' => 'DESC'));
            foreach($anos as $ano): ?>
                <li><?php echo $ano->name; ?></li>
            <?php endforeach; ?>
        </ul>
        <div class="bt-mobile bt-next"><i class="fas fa-chevron-right"></i></div>
    </div>

    <?php foreach($anos as $ano): ?>
        <div class="content-tabs">

            <div class="wrapper wrapper-head">
                <div> </div>
                <div>1T<?php echo $rest = substr($ano->name, -2);?></div>
                <div>2T<?php echo $rest = substr($ano->name, -2);?> </div>
                <div>3T<?php echo $rest = substr($ano->name, -2);?> </div>
                <div>4T<?php echo $rest = substr($ano->name, -2);?> </div>
            </div>


            <?php

            foreach ($financeiro[$ano->name] as $categoriaName => $categoriaItens):
                asort($financeiro[$ano->name][$categoriaName]); ?>

                <div class="wrapper">
                    <div><?php echo $categoriaName; ?></div>
                    <?php
                    if(isset($financeiro[$ano->name][$categoriaName]['1TR']) ||
                        isset($financeiro[$ano->name][$categoriaName]['2TR']) ||
                        isset($financeiro[$ano->name][$categoriaName]['3TR']) ||
                        isset($financeiro[$ano->name][$categoriaName]['4TR'])){ ?>
                        <?php if(isset($financeiro[$ano->name][$categoriaName]['1TR'])) { ?>
                            <div><a target="_blank" href="<?php echo wp_get_attachment_url(get_post_meta($financeiro[$ano->name][$categoriaName]['1TR'][0], 'financeiro', true)); ?>">
                                    <div class="show-mobile">1T<?php echo $rest = substr($ano->name, -2);?></div>
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/ico-download.png" alt=""></a>
                            </div>
                        <?php }else{ ?> <div  class="no-archive">
                            <div class="show-mobile">1T<?php echo $rest = substr($ano->name, -2);?></div>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/ico-download.png" alt="">
                        </div> <?php } ?>
                        <?php if(isset($financeiro[$ano->name][$categoriaName]['2TR'])) { ?>
                            <div><a target="_blank" href="<?php echo wp_get_attachment_url(get_post_meta($financeiro[$ano->name][$categoriaName]['2TR'][0], 'financeiro', true)); ?>">
                                    <div class="show-mobile">2T<?php echo $rest = substr($ano->name, -2);?></div>
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/ico-download.png" alt=""></a>
                            </div>
                        <?php }else{ ?> <div  class="no-archive">
                            <div class="show-mobile">2T<?php echo $rest = substr($ano->name, -2);?></div>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/ico-download.png" alt="">
                        </div> <?php } ?>
                        <?php if(isset($financeiro[$ano->name][$categoriaName]['3TR'])) { ?>
                            <div><a target="_blank" href="<?php echo wp_get_attachment_url(get_post_meta($financeiro[$ano->name][$categoriaName]['3TR'][0], 'financeiro', true)); ?>">
                                    <div class="show-mobile">3T<?php echo $rest = substr($ano->name, -2);?></div>
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/ico-download.png" alt=""></a>
                            </div>
                        <?php }else{ ?> <div  class="no-archive">
                            <div class="show-mobile">3T<?php echo $rest = substr($ano->name, -2);?></div>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/ico-download.png" alt="">
                        </div> <?php } ?>
                        <?php if(isset($financeiro[$ano->name][$categoriaName]['4TR'])) { ?>
                            <div><a target="_blank" href="<?php echo wp_get_attachment_url(get_post_meta($financeiro[$ano->name][$categoriaName]['4TR'][0], 'financeiro', true)); ?>">
                                    <div class="show-mobile">4T<?php echo $rest = substr($ano->name, -2);?></div>
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/ico-download.png" alt=""></a>
                            </div>
                        <?php }else{ ?> <div  class="no-archive">
                            <div class="show-mobile">4T<?php echo $rest = substr($ano->name, -2);?></div>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/ico-download.png" alt="">
                        </div> <?php } ?>
                        <?php
                    }
                    ?>
                </div>
            <?php endforeach; ?>

            <!-- anos-->
            <?php if(!empty($financeiro2[$ano->name])){ ?>
                <div class="wrapper-ano">
                <div class="wrapper wrapper-head">
                    <div> </div>
                    <div class="ano-name">AÑO <?php echo $ano->name;?></div>
                </div>

                    <?php foreach ($financeiro2[$ano->name] as $categoriaName1 => $categoriaItens){ ?>
                        <div class="wrapper ">
                            <div><?php echo $categoriaName1 ;?></div>

                            <?php foreach ($financeiro2[$ano->name][$categoriaName1] as $item){ ?>
                                <div class="descri">
                                        <a target="_blank" href="<?php echo wp_get_attachment_url(get_post_meta($item, 'financeiro', true)); ?>">
                                        <p>- <?php echo get_the_title($item); ?></p> <img src="<?php echo get_template_directory_uri(); ?>/images/ico-download.png" alt=""></a>
                                </div>
                            <?php } ?>

                        </div>
                   <?php } ?>

                </div>
           <?php } ?>
            <!-- anos-->

        </div>

    <?php endforeach; ?>
</div>



