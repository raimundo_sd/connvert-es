<div class="card-case">
    <div class="content-thumb">
        <a title="Ver conteúdo" class="thumb lazyload" href="<?php the_permalink(); ?>" alt="Conteúdo">
            <img src="<?php the_post_thumbnail_url('small'); ?>" alt="Conteúdo">
        </a>
    </div>
    <a title="Ver conteúdo" href="<?php the_permalink(); ?>">
        <h3><?php the_title(); ?></h3>
    </a>
</div>
