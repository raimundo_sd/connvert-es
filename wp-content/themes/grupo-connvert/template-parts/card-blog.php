<div class="col-12">
    <div class="item-post-blog">
        <div class="row">
            <div class="col-lg-6">
                <a href="<?php the_permalink(); ?>" class="image">
                    <?php the_post_thumbnail('full'); ?>
                </a>
            </div>

            <div class="col-lg-6">
                <div class="box-text">
                    <div class="infos-post">
                        <div class="categories">
                            <?php the_category(); ?>
                        </div>

                        <span class="date"><?php echo get_the_date("d/m/Y"); ?></span>
                    </div>

                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <?php the_excerpt(); ?>

                    <div class="box-author">
                        <img src="<?php echo get_avatar_url(get_the_author_meta('ID'), array('size' => 45)); ?>">
                        <div class="text-author">
                            <span class="title"><?php echo get_the_author_meta('display_name'); ?></span>
                            <span class="subtitle"><?php echo esc_attr( get_the_author_meta( 'cargo')); ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>