<?php get_header(); ?>
	<?php if(have_posts()): ?>
		<section class="more-posts">
			<div class="container">
				<h2 class="title">Resultados para: <?php echo get_search_query(); ?></h2>

				<div class="row">
					<?php while(have_posts()): ?>
                    <?php the_post(); ?>
						<div class="col-md-4">
							<?php get_template_part('template-parts/card-cases'); ?>
						</div>
					<?php endwhile; ?>
				</div>

				<div class="wrap-pagination">
					<div class="pagination">
						<?php wp_pagenavi(); ?>
					</div>
				</div>
			</div>
		</section>
	<?php else: ?>
		<section class="more-posts">
			<div class="container">
				<h2>Lo sentimos, no se encontraron publicaciones</h2>
			</div>
		</section>
	<?php endif; ?>

	<?php get_footer(); ?>

