<?php get_header(); ?>
<?php the_post(); ?>
	<section class="content-single-post">
		<div class="imagem">
            <?php the_post_thumbnail('full'); ?>
        </div>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="top-post">
						<?php $categorias = get_the_terms(get_the_ID(), 'category'); ?>
						<div class="list-category">
							<?php foreach($categorias as $categoria): ?>
								<a href="<?php echo get_term_link($categoria->term_id); ?>"><?php echo $categoria->name; ?></a>	
							<?php endforeach; ?>
						</div>
						<span class="date"><?php echo get_the_date("d/m/Y"); ?></span>
					</div>

					<h1><?php the_title(); ?></h1>

					<div class="top-author-blog">
						<img src="<?php echo get_avatar_url(get_the_author_meta('ID'), array('size' => 50)); ?>">

						<div class="text-author">
							<span class="title"><?php echo get_the_author_meta('display_name'); ?></span>
							<span class="subtitle"><?php echo esc_attr( get_the_author_meta( 'cargo')); ?></span>
						</div>

						<div class="social-author">
							<a href="https://twitter.com/intent/tweet?url=<?php the_permalink(); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
							<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>" target="_blank"><i class="fa fa-linkedin-square"></i></a>
							<a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank"><i class="fa fa-facebook-square"></i></a>
						</div>
					</div>

					<div class="box-content">
						<?php the_content(); ?>
					</div>

					<div class="list-tags">
						<?php foreach(get_the_terms(get_the_ID(), 'post_tag') as $categoria): ?>
							<a href="<?php echo get_term_link($categoria->term_id); ?>"><?php echo $categoria->name; ?></a>	
						<?php endforeach; ?>
					</div>

					<div class="full-author">
						<img src="<?php echo get_avatar_url(get_the_author_meta('ID'), array('size' => 80)); ?>">

						<div class="text-author">
							<span class="title"><?php echo get_the_author_meta('display_name'); ?></span>
							<?php echo wpautop(get_the_author_meta('description')); ?>
							<?php $post_author_id = get_post_field( 'post_author', $post->ID ); ?>
							<a href="<?php echo get_author_posts_url($post_author_id); ?>">Ver todas las publicaciones</a>
						</div>
					</div>

					<div class="post-links">
						<?php if($previous_post = get_previous_post()) : ?>
            				<a href="<?php echo get_the_permalink($previous_post->ID); ?>" class="prev-post"><?php echo get_the_title($previous_post->ID); ?></a>
            			<?php endif; ?>

            			<?php if($next_post = get_next_post()) : ?>
            				<a href="<?php echo get_the_permalink($next_post->ID); ?>" class="next-post"><?php echo get_the_title($next_post->ID); ?></a>
            			<?php endif; ?>
					</div>

					<div class="post-comments">
						<?php comments_template(); ?>
					</div>
				</div>

				<div class="col-lg-3">
					<aside class="side-categories">
						<span class="title">Categorías</span>

						<ul>
							<?php foreach(get_terms("category", array("parent" => 1)) as $getcat): ?>
								<li><a href="<?php echo get_term_link($getcat->term_id); ?>"><?php echo $getcat->name; ?></a></li>
							<?php endforeach; ?>
						</ul>
					</aside>
				</div>
			</div>
		</div>
	</section>
<?php get_footer(); ?>