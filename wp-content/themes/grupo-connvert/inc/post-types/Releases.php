<?php
/**
 * CUSTOM POST TYPE: Releases
 */

function releases_post_type() {

	$labels = array(
		'name'                => 'Releases',
		'singular_name'       => 'Release',
		'menu_name'           => 'Releases',
		'parent_item_colon'   => '',
		'all_items'           => 'Listagem',
		'view_item'           => 'Ver Release',
		'add_new_item'        => 'Adicionar Release',
		'add_new'             => 'Adicionar',
		'edit_item'           => 'Editar Release',
		'update_item'         => 'Atualizar Release',
		'search_items'        => 'Buscar Release',
		'not_found'           => 'Nenhum Release encontrado',
		'not_found_in_trash'  => 'Nenhum Release encontrado na lixeira',
	);

	$rewrite = array(
		'slug'                => 'releases',
		'with_front'          => false,
		'pages'               => false,
		'feeds'               => false,
	);

	$args = array(
		'label'               => 'releases',
		'description'         => 'Página com Releases',
		'labels'              => $labels,
		'supports'            => array('title', 'editor', 'thumbnail'),
		'taxonomies'          => array(),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 7,
		'menu_icon'           => 'dashicons-groups',
		'can_export'          => true,
		'has_archive'         => 'Releases',
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'post',
	);

	register_post_type( 'releases', $args );

}
add_action( 'init', 'releases_post_type', 0 );

new CustomAdminLogo('releases', 'f1ea', 'bold');

