<?php
/**
 * CUSTOM POST TYPE: Cases
 */

function cases_post_type() {
	$labels = array(
		'name'                => 'Cases',
		'singular_name'       => 'Case',
		'menu_name'           => 'Cases',
		'parent_item_colon'   => '',
		'all_items'           => 'Listagem',
		'view_item'           => 'Ver Case',
		'add_new_item'        => 'Adicionar Case',
		'add_new'             => 'Adicionar',
		'edit_item'           => 'Editar Case',
		'update_item'         => 'Atualizar Case',
		'search_items'        => 'Buscar Case',
		'not_found'           => 'Nenhum Case encontrado',
		'not_found_in_trash'  => 'Nenhum Case encontrado na lixeira',
	);

	$rewrite = array(
		'slug'                => 'cases',
		'with_front'          => false,
		'pages'               => false,
		'feeds'               => false,
	);

	$args = array(
		'label'               => 'cases',
		'description'         => 'Página com cases',
		'labels'              => $labels,
		'supports'            => array('title', 'thumbnail', 'editor', 'excerpt'),
		'taxonomies'          => array(),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 6,
		'menu_icon'           => 'dashicons-testimonial',
		'can_export'          => true,
		'has_archive'         => 'cases',
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'post',
	);

	register_post_type( 'cases', $args );

}
add_action( 'init', 'cases_post_type', 0 );

new CustomAdminLogo('cases', 'f0eb', 'bold');

add_action( 'add_meta_boxes', 'cases_metaboxes' );
function cases_metaboxes() {
    add_meta_box( 'cases_extras', 'Informações', 'cases_extras', 'cases', 'normal', 'default' );
}

function cases_extras() {
    global $post;
    create_nonce( $post, plugin_basename( __FILE__ ) );
    $subtitulo = get_post_meta($post->ID, 'subtitulo', true);

    ?>
    <div class="row my-3">
        <div class="col-md-12">
            <label class="field-label" for="subtitulo">subtitulo</label>
        </div>
        <div class="col-md-12">
            <input id="subtitulo" type="text" name="subtitulo" value="<?php echo $subtitulo; ?>" />
        </div>
    </div>
    <?php
}



function save_cases_extras($post_id, $post) {
    if(!verify_before_save_post_functions($post, plugin_basename(__FILE__)))
        return $post->ID;
    set_post_meta($post->ID, 'subtitulo');
}
add_action('save_post', 'save_cases_extras', 1, 2); // save the custom fields
    



