<?php

// Cria taxonomia categoria_materiais
function registra_tax_categoriaMateriais() {

	register_taxonomy(
		'categoria_materiais', array( 'materiais' ),
		array(
			'hierarchical' => true,
			'label' => 'Categoria',
			'show_ui' => true,
			'query_var' => true,
			'show_admin_column' => true,
			'labels' => array (
				'search_items' => 'Categoria',
				'popular_items' => 'Principais Categorias',
				'all_items' => 'Todas as Categorias',
				'edit_item' => 'Editar Categoria',
				'update_item' => 'Atualizar Categoria',
				'add_new_item' => 'Adicionar Categoria'
			),
			'sort' => true,
			'rewrite' => array( 'slug' => 'materiais', 'with_front' => false, 'hierarchical' => true ),
			'has_archive' => 'materiais'
		)
	);

}
add_action('init', 'registra_tax_categoriaMateriais');