<?php

// Cria taxonomia trimestres_Financeiro
function registra_tax_trimestresFinanceiro() {

	register_taxonomy(
		'trimestre_financeiro', array( 'financeiro' ),
		array(
			'hierarchical' => true,
			'label' => 'Trimestre',
			'show_ui' => true,
			'query_var' => true,
			'show_admin_column' => true,
			'show_in_menu' => 'edit.php?post_type=investidores',
			'labels' => array (
				'search_items' => 'Trimestre',
				'popular_items' => 'Principais Trimestres',
				'all_items' => 'Todas as Trimestres',
				'edit_item' => 'Editar Trimestre',
				'update_item' => 'Atualizar Trimestre',
				'add_new_item' => 'Adicionar Trimestre'
			),
			'sort' => true,
			'rewrite' => array( 'slug' => 'financeiro', 'with_front' => false, 'hierarchical' => true ), // modificado aqui, pra mostrar a hierarquia de url nas categorias
			'has_archive' => 'financeiro'
		)
	);

}
add_action('init', 'registra_tax_trimestresFinanceiro');