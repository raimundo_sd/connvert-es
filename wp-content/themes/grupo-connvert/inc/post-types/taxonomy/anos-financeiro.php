<?php

// Cria taxonomia anos-financeiro
function registra_tax_anosFinanceiro() {

	register_taxonomy(
		'anos_financeiro', array( 'financeiro' ),
		array(
			'hierarchical' => true,
			'label' => 'Ano',
			'show_ui' => true,
			'query_var' => true,
			'show_admin_column' => true,
			'labels' => array (
				'search_items' => 'Ano',
				'popular_items' => 'Principais Anos',
				'all_items' => 'Todas os Anos',
				'edit_item' => 'Editar Categoria',
				'update_item' => 'Atualizar Ano',
				'add_new_item' => 'Adicionar Ano'
			),
			'sort' => true,
			'rewrite' => array( 'slug' => 'financeiro', 'with_front' => false, 'hierarchical' => true ), // modificado aqui, pra mostrar a hierarquia de url nas categorias
			'has_archive' => 'financeiro'
		)
	);

}
add_action('init', 'registra_tax_anosFinanceiro');