<?php

// Cria taxonomia categoria_relatorios
function registra_tax_categoriaRelatorios() {

	register_taxonomy(
		'categoria_relatorios', array( 'relatorios' ),
		array(
			'hierarchical' => true,
			'label' => 'Categoria',
			'show_ui' => true,
			'query_var' => true,
			'show_admin_column' => true,
			'labels' => array (
				'search_items' => 'Categoria',
				'popular_items' => 'Principais Categorias',
				'all_items' => 'Todas as Categorias',
				'edit_item' => 'Editar Categoria',
				'update_item' => 'Atualizar Categoria',
				'add_new_item' => 'Adicionar Categoria'
			),
			'sort' => true,
			'rewrite' => array( 'slug' => 'relatorios', 'with_front' => false, 'hierarchical' => true ), // modificado aqui, pra mostrar a hierarquia de url nas categorias
			'has_archive' => 'relatorios'
		)
	);

}
add_action('init', 'registra_tax_categoriaRelatorios');