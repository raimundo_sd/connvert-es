<?php

define('DEFAULT_LANG','pt');

// Cria taxonomia language
add_action('init', 'language_taxonomy');
function language_taxonomy() {
    register_taxonomy(
        'language',
        array( 'cases','cliente','depoimento','post'),
        array( 'hierarchical' => true,
            'label' => 'Idioma',
            'show_ui' => true,
            'query_var' => true,
            'show_admin_column' => true,
            'labels' => array (
                'search_items' => __( 'Idioma', 'solucaodigital'),
                'popular_items' => 'Principais Idiomas',
                'all_items' => 'Todos os Idiomas',
                'edit_item' => 'Editar idioma',
                'update_item' => 'Atualizar Idioma',
                'add_new_item' => 'Adicionar Idioma',
                'menu_name' => __( 'Idioma', 'solucaodigital'),
            ),
            'sort' => true,
        )
    );
}

function custom_language_query_vars($vars) {
    $vars[] = 'language';
    return $vars;
}
add_filter( 'query_vars', 'custom_language_query_vars' );


add_action('save_post', 'assign_default_language', 10, 2);
function assign_default_language($post_id, $post){
	if(in_array($post->post_type, array( 'cases','cliente','depoimento','post')))
		if(!wp_get_post_terms($post_id, 'language' )) {
			$term_id = term_exists( DEFAULT_LANG, 'language');
			wp_set_post_terms( $post_id, $term_id, 'language', true );
		}
	return $post_id;
}

/**
 * @param WP_Query $query
 */
function language_query($query){
	if(is_admin())
		return $query;

	if(!isset($query->query_vars['language'])) {
		if ( in_array( strtolower( substr( $_SERVER['REQUEST_URI'], 0, 4 ) ), array( '/en', '/es', '/en/', '/es/' ) ) ) {
			$query->query_vars['language'] = substr( $_SERVER['REQUEST_URI'], 1, 2 );
		} else {
			$query->query_vars['language'] = DEFAULT_LANG;
		}
	}
	if(isset($query->query_vars['post_type']) && isset($query->query_vars['language']) && in_array($query->query['post_type'], array( 'cases','cliente','depoimento','post'))){
			$tax_query  = array(
				'taxonomy'         => 'language',
				'field'            => 'slug',
				'terms'            => array($query->query_vars['language']),
				'operator'         => 'IN'
			);
			$query->tax_query->queries[]    = $tax_query;
			$query->query_vars['tax_query'] = $query->tax_query->queries;
	}
	return $query;
}
add_action( 'pre_get_posts', 'language_query' );