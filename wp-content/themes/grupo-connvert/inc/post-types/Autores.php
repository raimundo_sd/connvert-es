<?php

/**
 * CUSTOM POST TYPE: Autores
 */


function autores_post_type() {

    $labels = array(
        'name'                => 'Autores',
        'singular_name'       => 'Autores',
        'menu_name'           => 'Autores',
        'parent_item_colon'   => '',
        'all_items'           => 'Listagem',
        'view_item'           => 'Ver Autor',
        'add_new_item'        => 'Adicionar Autor',
        'add_new'             => 'Adicionar',
        'edit_item'           => 'Editar Autor',
        'update_item'         => 'Atualizar Autor',
        'search_items'        => 'Buscar Autor',
        'not_found'           => 'Nenhum Autor encontrado',
        'not_found_in_trash'  => 'Nenhum Autor encontrado na lixeira',
    );

    $rewrite = array(
        'slug'                => 'autor',
        'with_front'          => false,
        'pages'               => true,
        'feeds'               => true,
    );

    $args = array(
        'label'               => 'autores',
        'description'         => 'Página de Autores',
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'thumbnail'),
        'taxonomies'          => array(),
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 4,
        'menu_icon'           => 'dashicons-id',
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => true,
        'publicly_queryable'  => true,
        'rewrite'             => $rewrite,
        'capability_type'     => 'post',
    );

    register_post_type( 'autores', $args );

}
add_action( 'init', 'autores_post_type', 0 );

function autor_url_rewrite() {
	// paginação do blog filtro por autor
	add_rewrite_rule('^autor/([^/]+)/page/(\d+)/?','index.php?post_type=post&autor=$matches[1]&paged=$matches[2]','top');

	// blog filtro autor
	add_rewrite_rule('^autor/([^/]+)/?','index.php?post_type=post&autor=$matches[1]','top');
}
add_action('init', 'autor_url_rewrite', 999 );

function custom_autor_query_var_filter($vars) {
	$vars[] = 'autor';
	return $vars;
}
add_filter( 'query_vars', 'custom_autor_query_var_filter' );

function autorSearch( $query ) {
	if(isset($query->query['post_type']) && $query->query['post_type'] == 'post' && get_query_var( 'autor' )) {
		$autor = sd_get_autor_by_name(get_query_var( 'autor' ));
		if($autor) {
			$meta_query = array(
				'key'   => 'autor',
				'value' => $autor->ID,
			);
			$query->set( 'meta_query', array( $meta_query ) );
			$query->meta_query->queries[] = $meta_query;
		}
	}
	return $query;
}
add_action( 'pre_get_posts', 'autorSearch' );


// inclui lista de autores nos posts
add_action( 'add_meta_boxes', 'autores_metaboxes' );
function autores_metaboxes() {
	add_meta_box('autores_select', 'Autor', 'autores_select', 'post', 'side', 'default');
	add_meta_box('autores_select', 'Autor', 'autores_select', 'conteudo', 'side', 'default');
}

function autores_select() {
	global $post;
	echo '<input type="hidden" name="autoresmeta_noncename" id="autoresmeta_noncename" value="' .
	     wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

	$autor = get_post_meta($post->ID, 'autor', true);
	echo '<select name="autor">';
	echo '<option>Selecione</option>';
	foreach ( listAutores() as $a) {
		$selected = ($a->ID == $autor) ? 'selected' : '';
		echo '<option value="'.$a->ID.'" '.$selected.'>'.$a->post_title.'</option>';
	}
	echo '</select>';
}

function save_autores_extras($post_id, $post) {
	if ( !wp_verify_nonce( $_POST['autoresmeta_noncename'], plugin_basename(__FILE__) ))
		return $post->ID;
	if ( !current_user_can( 'edit_post', $post->ID ))
		return $post->ID;

	set_post_meta($post_id, 'autor');
}
add_action('save_post', 'save_autores_extras', 1, 2);

function add_autor_cpt_sticky_column( $columns ) {
	unset($columns['author']);
	unset($columns['comments']);
	return array_merge( $columns,
		array( 'autores' => 'Autor' ) );
}
add_filter( 'manage_posts_columns' , 'add_autor_cpt_sticky_column' );

function posts_custom_columns( $column, $post_id ) {
	switch ( $column ) {
		case 'autores':
			$autorID = get_post_meta($post_id, 'autor', true);
			/** Para atualizar todos
			if(!is_numeric($autorID)) {
				$_POST['autor'] = 1201;
				set_post_meta( $post_id, 'autor' );
				$autorID = 1201;
			}**/
			if($autorID){
				$autor = get_post($autorID);
				echo $autor->post_title;
			}else{
				echo 'Nenhum autor selecionado';
			}
			break;
	}
}
add_action( 'manage_posts_custom_column' , 'posts_custom_columns', 10, 2 );
add_action( 'manage_conteudo_posts_custom_column' , 'posts_custom_columns', 10, 2 );

function listAutores(){
	global $wpdb;
	return $wpdb->get_results('SELECT ID, post_title, post_content, post_name FROM '.$wpdb->posts." WHERE post_type = 'autores' and post_status = 'publish'");
}

/**
 * @param false|int $post_id
 *
 * @return object|null
 */
function sd_get_autor($post_id=false){
	if(!$post_id){
		global $post;
		$post_id = $post->ID;
	}
	$autorID = get_post_meta($post_id, 'autor', true);
	if($autorID) {
		global $wpdb;
		return $wpdb->get_row( 'SELECT ID, post_title, post_content, post_name FROM ' . $wpdb->posts . " WHERE post_type = 'autores' and post_status = 'publish' AND ID = '".$autorID."'" );
	}
	return null;
}

function sd_get_autor_by_name($name){
	global $wpdb;
	return $wpdb->get_row( 'SELECT ID, post_title, post_content, post_name FROM ' . $wpdb->posts . " WHERE post_type = 'autores' and post_status = 'publish' AND post_name = '".$name."'" );
}