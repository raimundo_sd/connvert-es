<?php
/**
 * CUSTOM POST TYPE: Socioambientais
 */

function socioambientais_post_type() {

	$labels = array(
		'name'                => 'Socioambientais',
		'singular_name'       => 'Socioambiental',
		'menu_name'           => 'Relatório Socioambiental',
		'parent_item_colon'   => '',
		'all_items'           => 'Listagem',
		'view_item'           => 'Ver Relatório Socioambiental',
		'add_new_item'        => 'Adicionar Relatório Socioambiental',
		'add_new'             => 'Adicionar',
		'edit_item'           => 'Editar Relatório Socioambiental',
		'update_item'         => 'Atualizar Relatório Socioambiental',
		'search_items'        => 'Buscar Relatório Socioambiental',
		'not_found'           => 'Nenhum Relatório Socioambiental encontrado',
		'not_found_in_trash'  => 'Nenhum Relatório Socioambiental encontrado na lixeira',
	);

	$rewrite = array(
		'slug'                => 'socioambientais',
		'with_front'          => false,
		'pages'               => false,
		'feeds'               => false,
	);

	$args = array(
		'label'               => 'socioambientais',
		'description'         => 'Página com Relatórios Socioambientais',
		'labels'              => $labels,
		'supports'            => array('title'),
		'taxonomies'          => array(),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 8,
		'menu_icon'           => 'dashicons-groups',
		'can_export'          => true,
		'has_archive'         => 'socioambientais',
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'post',
	);

	register_post_type( 'socioambientais', $args );

}
add_action( 'init', 'socioambientais_post_type', 0 );


new CustomAdminLogo('socioambientais', 'f15c', 'bold');

new CustomFileUpload('socioambiental', 'Relatório Socioambiental- pdf', 'socioambientais', 'normal');


