<?php
/**
 * CUSTOM POST TYPE: Investidor
 */

function investidor_menu() {

	$labels = array(
		'name'                => 'Investidores',
		'singular_name'       => 'Investidor',
		'menu_name'           => 'Investidores'

	);





	$args = array(
        'label'               => 'investidores',
        'description'         => 'Página com investidores',
        'labels'              => $labels,
        'taxonomies'          => array(),
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 10,
        'menu_icon'           => 'dashicons-groups',
        'can_export'          => true,

        'capability_type'     => 'page',
	);

    register_post_type( 'investidores', $args );

}
add_action( 'init', 'investidor_menu', 0 );

new CustomAdminLogo('investidores', 'f1ea', 'bold');
