<?php
/**
 * CUSTOM POST TYPE: Executivos
 */

function executivos_post_type() {

	$labels = array(
		'name'                => 'Executivos',
		'singular_name'       => 'Executivo',
		'menu_name'           => 'Executivos',
		'parent_item_colon'   => '',
		'all_items'           => 'Listagem',
		'view_item'           => 'Ver Executivo',
		'add_new_item'        => 'Adicionar Executivo',
		'add_new'             => 'Adicionar',
		'edit_item'           => 'Editar Executivo',
		'update_item'         => 'Atualizar Executivo',
		'search_items'        => 'Buscar Executivo',
		'not_found'           => 'Nenhum Executivo encontrado',
		'not_found_in_trash'  => 'Nenhum Executivo encontrado na lixeira',
	);

	$rewrite = array(
		'slug'                => 'executivos',
		'with_front'          => false,
		'pages'               => false,
		'feeds'               => false,
	);

	$args = array(
		'label'               => 'executivos',
		'description'         => 'Página com Executivos',
		'labels'              => $labels,
		'supports'            => array('title', 'editor', 'thumbnail'),
		'taxonomies'          => array(),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 9,
		'menu_icon'           => 'dashicons-groups',
		'can_export'          => true,
		'has_archive'         => 'Executivos',
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'post',
	);

	register_post_type( 'executivos', $args );

}
add_action( 'init', 'executivos_post_type', 0 );

new CustomAdminLogo('executivos', 'f508', 'bold');


add_action( 'add_meta_boxes', 'executivos_metaboxes' );
function executivos_metaboxes() {
	add_meta_box( 'executivos_extras', 'Informações', 'executivos_extras', 'executivos', 'normal', 'default' );
}

function executivos_extras() {
	global $post;
	create_nonce( $post, plugin_basename( __FILE__ ) );
	$cargo = get_post_meta($post->ID, 'cargo', true);
	$linkedin = get_post_meta($post->ID, 'linkedin', true);
	?>    
    <div class="row my-3">
        <div class="col-md-3">
            <label class="field-label" for="cargo">Cargo/Setor</label>
        </div>
        <div class="col-md-9">
            <input id="cargo" type="text" class="widefat" name="cargo" value="<?php echo $cargo; ?>" />
        </div>
    </div>
	<div class="row my-3">
        <div class="col-md-3">
            <label class="field-label" for="linkedin">Linkedin</label>
        </div>
        <div class="col-md-9">
            <input id="linkedin" type="url" class="widefat" name="linkedin" value="<?php echo $linkedin; ?>" />
        </div>
    </div>
	<?php
}

function save_executivos_extras($post_id, $post) {
	if(!verify_before_save_post_functions($post, plugin_basename(__FILE__)))
		return $post->ID;
    set_post_meta($post->ID, 'cargo');
	set_post_meta($post->ID, 'linkedin');
    
}
add_action('save_post', 'save_executivos_extras', 1, 2); // save the custom fields
