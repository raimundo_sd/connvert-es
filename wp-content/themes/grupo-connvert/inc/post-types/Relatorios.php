<?php
/**
 * CUSTOM POST TYPE: Relatórios
 */

function relatorios_post_type() {

	$labels = array(
		'name'                => 'Relatórios',
		'singular_name'       => 'Relatório',
		'menu_name'           => 'Relatórios',
		'parent_item_colon'   => '',
		'all_items'           => 'Listagem',
		'view_item'           => 'Ver Relatório',
		'add_new_item'        => 'Adicionar Relatório',
		'add_new'             => 'Adicionar',
		'edit_item'           => 'Editar Relatório',
		'update_item'         => 'Atualizar Relatório',
		'search_items'        => 'Buscar Relatório',
		'not_found'           => 'Nenhum Relatório encontrado',
		'not_found_in_trash'  => 'Nenhum Relatório encontrado na lixeira',
	);

	$rewrite = array(
		'slug'                => 'relatorios',
		'with_front'          => false,
		'pages'               => false,
		'feeds'               => false,
	);

	$args = array(
		'label'               => 'relatorios',
		'description'         => 'Página com Relatórios',
		'labels'              => $labels,
		'supports'            => array('title'),
		'taxonomies'          => array(),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 8,
		'menu_icon'           => 'dashicons-groups',
		'can_export'          => true,
		'has_archive'         => 'Relatórios',
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'post',
	);

	register_post_type( 'relatorios', $args );

}
add_action( 'init', 'relatorios_post_type', 0 );


new CustomAdminLogo('relatorios', 'f15c', 'bold');

new CustomFileUpload('relatorio', 'Relatório - pdf', 'relatorios', 'normal');


