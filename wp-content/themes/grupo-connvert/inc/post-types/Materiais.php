<?php
/**
 * CUSTOM POST TYPE: Financeiro
 */

function materiais_post_type() {

	$labels = array(
		'name'                => 'Materiais',
		'singular_name'       => 'Material',
		'menu_name'           => 'Materiais',
		'parent_item_colon'   => '',
		'all_items'           => 'Materiais',
		'view_item'           => 'Ver Material',
		'add_new_item'        => 'Adicionar Material',
		'add_new'             => 'Adicionar',
		'edit_item'           => 'Editar Material',
		'update_item'         => 'Atualizar Material',
		'search_items'        => 'Buscar Material',
		'not_found'           => 'Nenhum Material encontrado',
		'not_found_in_trash'  => 'Nenhum Material encontrado na lixeira',
	);

	$rewrite = array(
		'slug'                => 'materiais',
		'with_front'          => false,
		'pages'               => false,
		'feeds'               => false,
	);

	$args = array(
		'label'               => 'materiais',
		'description'         => 'Página com Materiais',
		'labels'              => $labels,
		'supports'            => array('title'),
		'taxonomies'          => array('categoria_materiais'),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => 'edit.php?post_type=investidores',
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
        'menu_position'       => 11,
		'menu_icon'           => 'dashicons-groups',
		'can_export'          => true,
		'has_archive'         => 'materiais',
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'post',
	);

	register_post_type( 'materiais', $args );

}
add_action( 'init', 'materiais_post_type', 0 );

new CustomAdminLogo('materiais', 'f1ea', 'bold');

require_once __DIR__.'/taxonomy/categorias-materiais.php';


new CustomFileUpload('material', 'Material - pdf', 'materiais', 'normal');