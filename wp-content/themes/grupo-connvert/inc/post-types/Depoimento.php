<?php
/**
 * CUSTOM POST TYPE: Depoimento
 */
function depoimento_post_type() {

	$labels = array(
		'name'                => 'Depoimentos',
		'singular_name'       => 'Depoimento',
		'menu_name'           => 'Depoimentos',
		'parent_item_colon'   => '',
		'all_items'           => 'Listagem',
		'view_item'           => 'Ver Depoimento',
		'add_new_item'        => 'Adicionar Depoimento',
		'add_new'             => 'Adicionar',
		'edit_item'           => 'Editar Depoimento',
		'update_item'         => 'Atualizar Depoimento',
		'search_items'        => 'Buscar Depoimento',
		'not_found'           => 'Nenhum Depoimento encontrado',
		'not_found_in_trash'  => 'Nenhum Depoimento encontrado na lixeira',
	);

	$rewrite = array(
		'slug'                => 'depoimento',
		'with_front'          => false,
		'pages'               => false,
		'feeds'               => false,
	);

	$args = array(
		'label'               => 'depoimento',
		'description'         => 'Página com depoimento',
		'labels'              => $labels,
		'supports'            => array('title','editor', 'thumbnail'),
		'taxonomies'          => array('language'),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 8,
		'menu_icon'           => 'dashicons-groups',
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'post',
	);

	register_post_type( 'depoimento', $args );

}
add_action( 'init', 'depoimento_post_type', 0 );

function depoimento_admin_icon() {
	admin_icon('depoimento', 'f005');
}
add_action('admin_head', 'depoimento_admin_icon');