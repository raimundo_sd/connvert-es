<?php
/**
 * CUSTOM POST TYPE: Administrativo
 */

function administrativo_post_type() {

	$labels = array(
		'name'                => 'Administrativo',
		'singular_name'       => 'Administrativo',
		'menu_name'           => 'Relatório Administrativo',
		'parent_item_colon'   => '',
		'all_items'           => 'Relatórios Administrativos',
		'view_item'           => 'Ver Relatório Administrativo',
		'add_new_item'        => 'Adicionar Relatório Administrativo',
		'add_new'             => 'Adicionar',
		'edit_item'           => 'Editar Relatório Administrativo',
		'update_item'         => 'Atualizar Relatório Administrativo',
		'search_items'        => 'Buscar Relatório Administrativo',
		'not_found'           => 'Nenhum Relatório Administrativo encontrado',
		'not_found_in_trash'  => 'Nenhum Relatório Administrativo encontrado na lixeira',
	);

	$rewrite = array(
		'slug'                => 'administrativo',
		'with_front'          => false,
		'pages'               => false,
		'feeds'               => false,
	);

	$args = array(
		'label'               => 'administrativo',
		'description'         => 'Página com Relatórios Administrativos',
		'labels'              => $labels,
		'supports'            => array('title'),
		'taxonomies'          => array(),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => 'edit.php?post_type=investidores',
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 12,
		'menu_icon'           => 'dashicons-groups',
		'can_export'          => true,
		'has_archive'         => 'administrativo',
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'post',
	);

	register_post_type( 'administrativo', $args );

}
add_action( 'init', 'administrativo_post_type', 0 );


new CustomAdminLogo('administrativo', 'f15c', 'bold');

new CustomFileUpload('administrativo', 'Relatório Administrativo - pdf', 'administrativo', 'normal');


