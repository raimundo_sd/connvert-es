<?php
/**
 * CUSTOM POST TYPE: Cliente
 */
function cliente_post_type() {

	$labels = array(
		'name'                => 'Clientes',
		'singular_name'       => 'Cliente',
		'menu_name'           => 'Clientes',
		'parent_item_colon'   => '',
		'all_items'           => 'Listagem',
		'view_item'           => 'Ver Cliente',
		'add_new_item'        => 'Adicionar Cliente',
		'add_new'             => 'Adicionar',
		'edit_item'           => 'Editar Cliente',
		'update_item'         => 'Atualizar Cliente',
		'search_items'        => 'Buscar Cliente',
		'not_found'           => 'Nenhum Cliente encontrado',
		'not_found_in_trash'  => 'Nenhum Cliente encontrado na lixeira',
	);

	$rewrite = array(
		'slug'                => 'cliente',
		'with_front'          => false,
		'pages'               => false,
		'feeds'               => false,
	);

	$args = array(
		'label'               => 'cliente',
		'description'         => 'Página com cliente',
		'labels'              => $labels,
		'supports'            => array('title', 'thumbnail'),
		'taxonomies'          => array('language'),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 8,
		'menu_icon'           => 'dashicons-groups',
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'post',
	);

	register_post_type( 'cliente', $args );

}
add_action( 'init', 'cliente_post_type', 0 );

function cliente_admin_icon() {
	admin_icon('cliente', 'f500', 'bold');
}
add_action('admin_head', 'cliente_admin_icon');