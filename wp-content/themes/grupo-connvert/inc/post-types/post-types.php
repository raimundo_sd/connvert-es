<?php
/**
 * Solução Digital - arquivo com funções comuns aos post_types
 *
 * @package solucaodigital
 */
// Carregamento dos custom post types
// ex: require_once 'Cases.php';
    require_once __DIR__ . '/Conteudo.php';
    require_once __DIR__. '/Cases.php';
	require_once __DIR__ . '/Socioambiental.php';
	require_once __DIR__. '/Executivos.php';
	require_once  __DIR__. '/Investidor.php';
    require_once  __DIR__. '/Materiais.php';
    require_once  __DIR__. '/Administrativo.php';
    require_once  __DIR__. '/Financeiro.php';
	require_once  __DIR__. '/Autores.php';

function set_post_meta($post_id, $meta_name, $request_name=false){
	$name_to_get_on_POST = $meta_name;
	if($request_name)
		$name_to_get_on_POST = $request_name;
	if (metadata_exists('post', $post_id, $meta_name)) { // If the custom field already has a value
		update_post_meta( $post_id, $meta_name, $_POST[$name_to_get_on_POST] );
	} else { // If the custom field doesn't have a value
		add_post_meta( $post_id, $meta_name, $_POST[$name_to_get_on_POST] );
	}
}
function create_nonce($post, $basename){
	echo '<input type="hidden" name="'.$post->post_type.'meta_noncename" id="'.$post->post_type.'meta_noncename" value="'.wp_create_nonce($basename) . '" />';
}
function load_custom_wp_admin_style() {
	wp_enqueue_style('font-awesome', get_template_directory_uri() . '/inc/assets/css/fontawesome-all.css' );
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );
function verify_before_save_post_functions($post, $basename){
	//Verify nonce
	if ( !wp_verify_nonce( $_POST[$post->post_type.'meta_noncename'], $basename ))
		return false;
	// Verify if the user is allowed to edit the post or page
	if ( !current_user_can( 'edit_post', $post->ID ))
		return false;
	return true;
}
function admin_icon($post_type, $fa_code, $fa_weight = '400', $fa_size = '18px', $fa_font='fontAwesome') {
	echo "<style type='text/css' media='screen'>
	       #adminmenu .menu-icon-".$post_type." div.wp-menu-image:before {
	            font-family:  '".$fa_font."', sans-serif !important;
	            content: '\\".$fa_code."'; 
	            font-weight: ".$fa_weight.";
	            font-size: ".$fa_size.";
	        }
     	</style>";
}
function get_the_first_term_name($post_id, $taxonomy){
	$terms = get_the_terms($post_id, $taxonomy);
	if(isset($terms[0]))
		return $terms[0]->name;
	return '';
}