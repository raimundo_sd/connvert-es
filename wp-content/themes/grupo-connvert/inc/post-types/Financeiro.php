<?php
/**
 * CUSTOM POST TYPE: Financeiro
 */

function financeiro_post_type() {

	$labels = array(
		'name'                => 'Financeiro',
		'singular_name'       => 'Financeiro',
		'menu_name'           => 'Informações Financeiras',
		'parent_item_colon'   => '',
		'all_items'           => 'Informações Financeiras',
		'view_item'           => 'Ver Financeiro',
		'add_new_item'        => 'Adicionar Financeiro',
		'add_new'             => 'Adicionar',
		'edit_item'           => 'Editar Financeiro',
		'update_item'         => 'Atualizar Financeiro',
		'search_items'        => 'Buscar Financeiro',
		'not_found'           => 'Nenhum Financeiro encontrado',
		'not_found_in_trash'  => 'Nenhum Financeiro encontrado na lixeira',
	);

	$rewrite = array(
		'slug'                => 'financeiro',
		'with_front'          => false,
		'pages'               => false,
		'feeds'               => false,
	);

	$args = array(
		'label'               => 'financeiro',
		'description'         => 'Página com Financeiro',
		'labels'              => $labels,
		'supports'            => array('title'),
		'taxonomies'          => array(''),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => 'edit.php?post_type=investidores',
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
        'menu_position'       => 10,
		'menu_icon'           => 'dashicons-groups',
		'can_export'          => true,
		'has_archive'         => 'Financeiro',
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'post',
	);

	register_post_type( 'financeiro', $args );

}
add_action( 'init', 'financeiro_post_type', 0 );


new CustomAdminLogo('financeiro', 'f1ea', 'bold');

require_once __DIR__.'/taxonomy/anos-financeiro.php';
require_once __DIR__.'/taxonomy/trimestres-financeiro.php';
require_once __DIR__.'/taxonomy/categorias-financeiro.php';


new CustomFileUpload('financeiro', 'Informação Financeira- pdf', 'financeiro', 'side');
//add_menu_page('Investidores', 'Investidores', 'manage_options', 'investidores', 'função', 'dashicons-admin-post', 2);