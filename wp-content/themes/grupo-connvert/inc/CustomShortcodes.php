<?php

function listBreadcrumb(){
	$return = '';
	if ( function_exists('yoast_breadcrumb') ) {
		$return.=  yoast_breadcrumb( '<div id="breadcrumbs">','</div>' );
	}
	return $return;
}
add_shortcode('breadcrumb', 'listBreadcrumb');

function listFinanceiro(){
    get_template_part('template-parts/list-documentos-financeiros');
};
add_shortcode('listFinanceiro', 'listFinanceiro');

function listMateriais(){
    get_template_part('template-parts/list-materiais');
};
add_shortcode('listMateriais', 'listMateriais');

function listAdministrativo(){
    get_template_part('template-parts/card-administrativo');
};
add_shortcode('listAdministrativo', 'listAdministrativo');



new CustomShortcodes('listCases',
	array(
		'post_type' => 'cases',
		'list_class' => 'carousel-conteudo',
		'template_part' => 'template-parts/card-cases',
	)
);

new CustomShortcodes('listReleases',
	array(
		'post_type' => 'conteudo',
		'list_class' => 'carousel-conteudo',
		'template_part' => 'template-parts/card-cases',
        'orderby' => 'date',
		'order' =>  'DESC'
	)
);

new CustomShortcodes('listReleasesInvestimentos',
    array(
        'post_type' => 'conteudo',
        'list_class' => 'row investidor-posts',
        'columns' => 'col-md-4',
        'template_part' => 'template-parts/card-cases',
        'posts_per_page' => 3
    )
);

new CustomShortcodes('listPosts',
	array(
		'post_type' => array('conteudo','cases'),
		'list_class' => 'row home-posts',
		'columns' => 'col-md-4',
		'template_part' => 'template-parts/card-posts',
		'posts_per_page' => 3,
		'orderby' => 'date',
		'order' =>  'DESC'
	)
);

new CustomShortcodes('listExecutivos',
	array(
		'post_type' => 'executivos',
		'list_class' => 'row executivos',
		'columns' => 'col-md-4',
        'orderby' => 'title',
        'order' =>  'ASC'

	)
);

new CustomShortcodes('listSocioambientais',
	array(
		'post_type' => 'socioambientais',
		'list_class' => 'row list-administrativos',
		'columns' => 'col-md-12',
        'template_part' => 'template-parts/card-socioambiental'
	)
);

new CustomShortcodes('listRelatoriosSocioambientais',
    array(
        'post_type' => 'socioambientais',
        'list_class' => 'row ',
        'columns' => 'col-md-4',
        'template_part' => 'template-parts/card-relatorios-socioambiental',
        'posts_per_page' => 4
    )
);


class CustomShortcodes {
	public $shortCode;
	public $post_type = false;
	public $list_class = false;
	public $post_status = 'publish';
	public $posts_per_page = -1;
	public $columns = false;
	public $template_part = false;
	public $script = false;
	public $order= false;
	public $orderby= false;


	//Se quiser chamar uma função custom
	public $callback = false;


	public function __construct($shortCode, $params = array('callback' => false, 'post_type' => false, 'list_class' => false)){
		$this->shortCode = $shortCode;
		$this->setAttr($params);
		if(!$this->callback)
			$this->callback = array($this, 'shortCode');

		add_shortcode( $this->shortCode, $this->callback );
	}

	public function setAttr($attributes = array()){
		foreach($attributes as $key => $value){
			if(property_exists(get_class($this), $key))
				$this->$key = $value;
		}
		return $this;
	}

	public function shortCode(){
		if($this->post_type) {

			$the_query = new WP_Query( array(
				'post_type'      => $this->post_type,
				'post_status'    => $this->post_status,
				'posts_per_page' => $this->posts_per_page,
                'orderby'           =>$this->orderby,
                'order'           =>$this->order

			) );
			if(is_array($this->post_type))
				$this->post_type = implode('_', $this->post_type);
			if(!$this->list_class)
				$this->list_class = $this->post_type.'-list';
			if(!$this->template_part)
				$this->template_part = 'template-parts/card-'.$this->post_type;



			$retorno   = '';
			if ( $the_query->have_posts() ) {
				$retorno .= '<div class="'.$this->list_class.'">';
				while ( $the_query->have_posts() ) {
					$the_query->the_post();
					if($this->columns)
						$retorno .='<div class="'.$this->columns.'">';
					//Card
					ob_start();
					get_template_part($this->template_part);
					$retorno .= ob_get_contents();
					ob_end_clean();

					if($this->columns)
						$retorno .= '</div>';
				}
				$retorno .= '</div>';
			}
			wp_reset_query();

			if($this->script)
				$retorno .= '<script>'.$this->script.'</script>';

			return $retorno;
		}
	}


}
