<?php


class CustomAdminLogo {
	public $label;
	public $code;
	public $weight;
	public $fontSize;

	public function __construct($label, $fontAwesome4DigitCode, $fontWeight = 'bold', $fontSize = '16px'){
		$this->label = $label;
		$this->code = $fontAwesome4DigitCode;
		$this->weight = $fontWeight;
		$this->fontSize = $fontSize;
		add_action('admin_head', array($this, 'admin_icon'));
	}

	function admin_icon() {
		echo "<style type='text/css' media='screen'>
	       #adminmenu .menu-icon-".$this->label." div.wp-menu-image:before {
	            font-family:  'fontAwesome', sans-serif !important;
	            content: '\\".$this->code."'; 
	            font-weight: ".$this->weight.";
	            font-size: ".$this->fontSize.";
	        }
     	</style>";
	}

}