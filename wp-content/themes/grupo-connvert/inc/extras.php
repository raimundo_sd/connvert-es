<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package solucaodigital
 */


/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function sd_pingback_header() {
    if ( is_singular() && pings_open() ) {
        echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
    }
}
add_action( 'wp_head', 'sd_pingback_header' );

/**
 * @param $id
 * @param $size = full / medium
 * @return string
 *
 * Função para ajustar o tamanho das imagens de acordo com o dispositivo onde ela será mostrada
 * Melhora a performance do site em dispositivos móveis
 * usar: <?php echo show_responsive_image(get_the_ID(), 'full'); ?>
 */
function show_responsive_image( $id, $size ) {
    $thumb = get_the_post_thumbnail_url($id);
    if($thumb){
        $attID = get_post_thumbnail_id($id);
    }else{
        $image = wp_get_attachment_image_src($id, $size);
        $thumb = $image[0];
        $attID = $id;
    }
    $srcset_value = wp_get_attachment_image_srcset($attID);
    $srcset = $srcset_value ? ' srcset="' . esc_attr( $srcset_value ) . '"' : '';
    $sizes_value = wp_get_attachment_image_sizes( $attID, $size );
    $sizes = $sizes_value ? ' sizes="' . esc_attr( $sizes_value ) . '"' : '';

    return '<img data-lazy="'.$thumb.'" '.$srcset.' '.$sizes.' width="100%" height="auto">';
}

/**
 * Customização do login
 */
function sd_custom_login_logo() {
    echo '
    <style type="text/css">
    .login h1 a {
        background-image:url('.get_bloginfo('template_directory').'/images/logo-cliente.png) !important;
        background-size: 200px 39px;
        width:330px;
        height: 50px;}
    body.login, html {background-color:#FFFFFF !important;}
    .wp-core-ui .button-primary {color:#fff;background:#002d63;box-shadow: none;border:none;text-shadow: none;}
    .wp-core-ui .button-primary:hover {color:#fff;background:#002d63;box-shadow: none;outline:0 !important;}
    .wp-core-ui .button-primary:active {color:#fff;background:#002d63;box-shadow: none;outline:0 !important;}
    .wp-core-ui .button-primary:focus{color:#fff;background:#002d63;box-shadow: none;outline:0 !important;}
    #wp-auth-check-wrap #wp-auth-check {background:#001f44;}
    .login .message {border-left: 4px solid #001f44;}
    .login form {box-shadow:none;border:solid 1px #001f44;}
    .logo-sd:focus,.logo-sd:hover{outline:0 !important;}
    .login #nav a,.login #backtoblog a{color:#001f44;}
    .login #nav a:hover,.login #backtoblog a:hover{color:#001f44;text-decoration:underline;}
    </style>';
}
add_action('login_head', 'sd_custom_login_logo');

function sd_logo() { ?>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            document.getElementById('nav').innerHTML = '<p style="width:100%; text-align: center;"><a class="logo-sd" href="https://solucao.digital" target="_blank" title="Solução Digital - Agência Digital"><img alt="Solução Digital - Agência Digital" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo-sd-admin.png"></a></p>' + document.getElementById('nav').innerHTML;
        });
    </script>
<?php }
add_action( 'login_enqueue_scripts', 'sd_logo' );


/**
 * @param $user
 * Adicionar campo CARGO para o Author
 */
function extra_user_profile_fields( $user ) {
	?>
    <div class="cliente-info">
        <table class="form-table">
            <tr>
                <th><label for="empresa">Oficina</label></th>
                <td>
                    <input type="text" name="cargo" id="cargo" value="<?php echo esc_attr( get_the_author_meta( 'cargo', $user->ID ) ); ?>" class="regular-text" /><br />
                    <span class="description">Posición de usuario para usar en el perfil</span>
                </td>
            </tr>
        </table>
    </div>
<?php }
add_action( 'show_user_profile', 'extra_user_profile_fields');
add_action( 'edit_user_profile', 'extra_user_profile_fields');
add_action( "user_new_form", 'extra_user_profile_fields');
add_action( 'personal_options_update','save_extra_user_profile_fields');
add_action( 'edit_user_profile_update', 'save_extra_user_profile_fields');

function save_extra_user_profile_fields( $user_id ) {
	if ( !current_user_can( 'edit_user', $user_id ) ) {
		return false;
	}
	update_user_meta( $user_id, 'cargo', $_POST['cargo'] );
}


/**
 * @param $postID
 *
 * Contador de views dos posts, pra mostrar os mais lidos
 * basta adicionar a chamada da função na single dessa forma: sd_post_views(get_the_ID());
 *
 */
function sd_post_views($postID = null) {
    if($postID == null)
        $postID = get_the_ID();
    if($postID) {
	    $count_key = 'sd_post_views_count';
	    $count = get_post_meta( $postID, $count_key, true );
	    if ( $count == '' ) {
		    delete_post_meta( $postID, $count_key );
		    add_post_meta( $postID, $count_key, '0' );
	    } else {
		    $count ++;
		    update_post_meta( $postID, $count_key, $count );
	    }
    }
}

/**
 * Adiciona Restrição aos usuários
 */
function remove_menus () {
	global $menu;
	global $current_user;

	$uid = get_current_user_id();
	if($uid == 4) {

		$restricted = array(
			__( 'Settings' ),
			__( 'Plugins' )
		);

		remove_menu_page( 'amp-options' ); // AMP
		remove_menu_page( 'duplicator' ); // Duplicator
		remove_menu_page( 'vc-general' ); // vc
		remove_menu_page( 'vc-welcome' ); // VC
		remove_menu_page( 'edit.php?post_type=page' ); // pages
		remove_menu_page( 'edit.php?post_type=cases' ); // cases
		remove_menu_page( 'edit.php?post_type=autores' ); // autores
		remove_menu_page( 'edit.php?post_type=socioambientais' ); // socioambiental
		remove_menu_page( 'edit.php?post_type=executivos' ); // executivos
		remove_menu_page( 'edit.php?post_type=conteudo' ); // conteudo
		remove_menu_page( 'edit.php' ); //posts
		remove_menu_page( 'upload.php' ); //midias
		remove_menu_page( 'edit-comments.php' ); // comments
		remove_menu_page( 'activity_log_page' ); // Registro de Atividades
		remove_menu_page( 'plugins.php' ); // Plugins
		remove_menu_page( 'tools.php' ); // Ferramentas
		remove_menu_page( 'options-general.php' ); // Configurações
		remove_submenu_page( 'index.php', 'update-core.php' );
		remove_submenu_page( 'themes.php', 'theme-editor.php' );
		if ( is_array( $menu ) ) {
			end( $menu );
			while ( prev( $menu ) ) {
				$value = explode( ' ', $menu[ key( $menu ) ][0] );
				if ( in_array( $value[0] != null ? $value[0] : "", $restricted ) ) {
					unset( $menu[ key( $menu ) ] );
				}
			}
		}
	}
}
add_action('admin_menu', 'remove_menus');
add_action( 'admin_init', 'remove_menus' );