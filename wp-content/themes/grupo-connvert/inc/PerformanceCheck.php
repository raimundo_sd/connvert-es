<?php
if(!function_exists('themeslug_sanitize_checkbox')) {
	function themeslug_sanitize_checkbox( $checked ) {
		// Boolean check.
		return ( ( isset( $checked ) && true == $checked ) ? true : false );
	}
}
function performanceCheck(){
	if(current_user_can( 'administrator' ) && get_theme_mod('showPerformance')){
		echo '<div style="position: fixed;bottom: 0;left: 0;width: 175px;padding: 15px;font-size: 15px;background-color: #FFF;border-top: 1px solid #EEE;border-left: 1px solid #EEE;z-index: 999;">
        Start: '.$_SERVER["REQUEST_TIME_FLOAT"];
		$end = microtime(true);
		echo '<br>End: '.$end;
		$total = $end - $_SERVER["REQUEST_TIME_FLOAT"];
		if($total > 0.80){
			$total = '<span style="color:#bc1616">' . $total . '</span>';
		}elseif ($total > 0.50){
			$total = '<span style="color:#c4a615">' . $total . '</span>';
		}else{
			$total = '<span style="color:#16c335">' . $total . '</span>';
		}
		echo "<br><b>Total time:</b> ". ( $total ).
		     '</div>';
	}
}
add_action('wp_footer', 'performanceCheck');

function performance_check_customize_register( $wp_customize ) {
	$wp_customize->add_setting( 'showPerformance', array(
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'themeslug_sanitize_checkbox',
	) );

	$wp_customize->add_control( 'showPerformance', array(
		'type' => 'checkbox',
		'section' => 'title_tagline', // Add a default or your own section
		'label' => __( 'Mostrar Performance' ),
		'description' => __( 'Ao ativar, será exibido no frontend o tempo de processamento do site.' ),
	) );
}
add_action( 'customize_register', 'performance_check_customize_register' );