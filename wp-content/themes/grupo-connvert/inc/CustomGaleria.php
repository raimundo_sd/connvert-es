<?php

class CustomGaleria {

	public $metaValue;
	public $metaBoxTitle;
	public $post_type;
	public $sectionTitle;
	public $sectionChamada;

	public function __construct($metaValue, $metaBoxTitle, $post_type, $sectionTitleMetaValue = false, $sectionChamadaMetaValue = false) {
		$this->metaValue = sanitize_title($metaValue);
		$this->metaBoxTitle = $metaBoxTitle;
		$this->post_type = $post_type;
		$this->sectionTitle = $sectionTitleMetaValue;
		$this->sectionChamada = $sectionChamadaMetaValue;
		add_action( 'admin_enqueue_scripts', array('CustomGaleria', 'custom_galeria_enqueue_admin_scripts'));
		add_action( 'save_post', array($this, 'save_metaValue'), 1, 2); // save the custom fields
		add_action( 'add_meta_boxes', array($this, 'set_metabox' ));
	}

	public function set_metabox(){
		add_meta_box($this->post_type.'_'.$this->metaValue, $this->metaBoxTitle, array($this, 'the_metabox'), $this->post_type, 'normal', 'default');
	}

	public static function custom_galeria_enqueue_admin_scripts() {
		wp_enqueue_style( 'cg_jqueryUi_CSS', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css', false );
		wp_enqueue_script( 'cg_jqueryUi_JS', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js', false );
	}


	public function save_metaValue($post_id, $post) {
		if ( !wp_verify_nonce( $_POST[$post->post_type.'_gallery_nonce'], plugin_basename(__FILE__) ))
			return $post->ID;

		if ( !current_user_can( 'edit_post', $post->ID ))
			return false;

		set_post_meta($post->ID, $this->metaValue);

		if($this->sectionTitle)
			set_post_meta($post->ID, $this->sectionTitle);
		if($this->sectionChamada)
			set_post_meta($post->ID, $this->sectionChamada);
	}

	public function the_metabox(){
		global $post;
		if($this->sectionTitle) {
			echo '<input class="componentTitleInput" type="text" name="'.$this->sectionTitle.'" value="'.get_post_meta($post->ID, $this->sectionTitle, true).'"  placeholder="Título da seção" />';
		}
		if($this->sectionChamada) {
			wp_enqueue_editor();
			echo '<div><label style="font-weight: 600;">Chamada da seção:</label></div><div><textarea id="'.$this->sectionChamada.'" name="'.$this->sectionChamada.'">'.get_post_meta($post->ID, $this->sectionChamada, true).'</textarea></div>';
			echo "<script type='text/javascript'>jQuery(document).ready(function($){wp.editor.initialize('".$this->sectionChamada."');});</script>";
			echo '<br><hr>';
			echo '<label style="font-weight: 600;">Imagens '.$this->metaBoxTitle.'</label>';
		}
		$fotos = get_post_meta($post->ID, $this->metaValue, true);
		echo '<input type="hidden" name="'.$post->post_type.'_gallery_nonce" id="'.$post->post_type.'_gallery_nonce" value="'.wp_create_nonce(plugin_basename(__FILE__)) . '" />';
		$this->cgCss();
		?>
		<div class="metabox">
            <ul class="agdfcss admin-galeria-de-fotos-<?php echo $this->metaValue; ?>">
                <?php
                if($fotos){
                    foreach ($fotos as $foto) {
                        ?>
                        <li>
                            <img src="<?php echo wp_get_attachment_image_url( $foto ); ?>"/>
                            <input type="hidden" name="<?php echo $this->metaValue; ?>[]" value="<?php echo $foto; ?>"/>
                            <div><i class="fa fa-edit edit-img-<?php echo $this->metaValue; ?>" img="<?php echo $foto; ?>"></i> <i class="fa fa-times remove-galery-img-<?php echo $this->metaValue; ?>" img="<?php echo $foto; ?>"></i></div>
                            <p class="textFile"><?php echo basename ( get_attached_file( $foto ) ); ?></p>
                        </li>
                        <?php
                    }
                }
                ?>
                <li><div class="agdfcss-add add-imagem-galeria-<?php echo $this->metaValue; ?>"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/add.png" /></div></li>
            </ul>
		</div>
		<script type='text/javascript'>
            jQuery(document).ready(function($){
                function admin_imagem_galeria_<?php echo $this->metaValue; ?>(button_class) {
                    var _custom_media = true;
                    $('body').on('click', button_class, function(e) {
                        var button = $(button_class);
                        _custom_media = true;
                        wp.media.editor.send.attachment = function(props, attachment){
                            if ( _custom_media ) {
                                $('.admin-galeria-de-fotos-<?php echo $this->metaValue; ?> li:last').before('<li><img src="'+attachment.url+'" /><input type="hidden" name="<?php echo $this->metaValue; ?>[]" value="'+attachment.id+'" /><div><i class="fa fa-edit edit-img-<?php echo $this->metaValue; ?>" img="'+attachment.id+'"></i> <i class="fa fa-times remove-galery-img-<?php echo $this->metaValue; ?>" img="'+attachment.id+'"></i></div><p class="textFile">'+attachment.filename+'</p></li>');
                                remove_imagem_galeria_<?php echo $this->metaValue; ?>();
                                ordernar_imagem_galeria_<?php echo $this->metaValue; ?>();
                                edit_imagem_galeria_<?php echo $this->metaValue; ?>();
                            }
                        }
                        wp.media.editor.open(button);
                        return false;
                    });
                }
                admin_imagem_galeria_<?php echo $this->metaValue; ?>('.add-imagem-galeria-<?php echo $this->metaValue; ?>');
                function remove_imagem_galeria_<?php echo $this->metaValue; ?>(){
                    $('.remove-galery-img-<?php echo $this->metaValue; ?>').off().on('click', function () {
                        $(this).parent().parent().remove();
                    });
                }
                function edit_imagem_galeria_<?php echo $this->metaValue; ?>(){
                    $('.edit-img-<?php echo $this->metaValue; ?>').off().on('click', function () {
                        var button = $(this);
                        var replace = $(this).parent().parent()

                        custom_uploader = wp.media.frames.file_frame = wp.media({
                            title: 'Editar imagem',
                            button: {
                                text: 'Confirmar'
                            },
                            multiple: false
                        });
                        custom_uploader.on('select', function() {
                            var attachment = custom_uploader.state().get('selection').first().toJSON();
                            replace.html('<img src="'+attachment.url+'" /><input type="hidden" name="<?php echo $this->metaValue; ?>[]" value="'+attachment.id+'" /><div><i class="fa fa-edit edit-img-<?php echo $this->metaValue; ?>" img="'+attachment.id+'"></i> <i class="fa fa-times remove-galery-img-<?php echo $this->metaValue; ?>" img="'+attachment.id+'"></i></div><p class="textFile">'+attachment.filename+'</p>');
                            remove_imagem_galeria_<?php echo $this->metaValue; ?>();
                            ordernar_imagem_galeria_<?php echo $this->metaValue; ?>();
                            edit_imagem_galeria_<?php echo $this->metaValue; ?>();
                        });
                        custom_uploader.on('open', function() {
                            var selection = custom_uploader.state().get( 'selection' );
                            selection.add( wp.media.attachment( $(button).attr('img') ) );
                        });
                        custom_uploader.open();
                        return false;
                    });
                }

                function ordernar_imagem_galeria_<?php echo $this->metaValue; ?>(){
                    jQuery('.admin-galeria-de-fotos-<?php echo $this->metaValue; ?>').sortable();
                }

                remove_imagem_galeria_<?php echo $this->metaValue; ?>();
                ordernar_imagem_galeria_<?php echo $this->metaValue; ?>();
                edit_imagem_galeria_<?php echo $this->metaValue; ?>();
            });
		</script>
		<?php
	}

	public function cgCss(){
		?>
		<style>
            div.mce-panel {
                border: 0.1px solid #DDD;
            }
            .componentTitleInput {
                font-size: 26px;
                border-left: none !important;
                border-top: none !important;
                border-right: none !important;
                box-shadow: none !important;
                margin-bottom: 20px;
                font-weight: 500;
                width: 95%;
            }
            .metabox {
                display: flex;
            }
			.agdfcss li {
                width: 120px;
                height: 120px;
                background-color: #EEE;
                list-style: none;
                margin: 10px;
                float: left;
                cursor: move;
                margin-bottom: 55px;
			}
            .agdfcss > li > div {
                height: 20px;
                width: 40px;
                position: absolute;
                margin-top: -125px;
                margin-left: 80px;
                cursor: pointer;
                line-height: 20px;
                text-align: center;
            }
            .agdfcss > li > div > .fa-edit {
                font-size: 14px;
                color: #172791;
                background-color: #FFF;
                width: 15px;
                height: 15px;
                border-radius: 5px;
                opacity: 0.8;
                padding: 2px 0 0 2px;
            }
            .agdfcss > li > div > .fa-times {
                font-size: 14px;
                color: #F00;
                background-color: #FFF;
                width: 15px;
                height: 15px;
                border-radius: 5px;
                opacity: 0.8;
                padding: 1px;
            }
			.agdfcss > li > div > .fa:hover {
				opacity: 1;
			}

			.agdfcss > li > div:hover {
				opacity: 1;
			}
			.agdfcss > li > .add-imagem-galeria {
				margin: 0;
				opacity: 1;
				padding: 35px 27px;
			}
			.agdfcss > li > div > img {
				width: 100%;
				height: 100%;
				object-fit: contain;
			}
			.agdfcss > li > img {
				width: 100%;
				height: 100%;
				opacity: 0.8;
				object-fit: contain;
			}
			.agdfcss > li > input {
				display: none;
			}
            .agdfcss .agdfcss-add {
                margin: 32px;
                width: 55px;
                height: 55px;
            }
            .agdfcss .textFile {
                overflow: hidden;
                text-overflow: ellipsis;
                margin-top: 0;
                font-size: 10px;
                height: 45px;
            }
		</style>
		<?php
	}

}