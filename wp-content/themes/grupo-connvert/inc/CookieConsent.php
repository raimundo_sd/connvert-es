<?php
class CookieConsent {

	function menu_admin(){
		add_menu_page('LGPD - Cookie', 'LGPD - Cookie', 'manage_options', 'lgpd-cookie-consent', array('CookieConsent', 'adminOptions'), 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBzdGFuZGFsb25lPSJubyI/Pgo8IURPQ1RZUEUgc3ZnIFBVQkxJQyAiLS8vVzNDLy9EVEQgU1ZHIDIwMDEwOTA0Ly9FTiIKICJodHRwOi8vd3d3LnczLm9yZy9UUi8yMDAxL1JFQy1TVkctMjAwMTA5MDQvRFREL3N2ZzEwLmR0ZCI+CjxzdmcgdmVyc2lvbj0iMS4wIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiB3aWR0aD0iNzAwLjAwMDAwMHB0IiBoZWlnaHQ9IjcwMC4wMDAwMDBwdCIgdmlld0JveD0iMCAwIDcwMC4wMDAwMDAgNzAwLjAwMDAwMCIKIHByZXNlcnZlQXNwZWN0UmF0aW89InhNaWRZTWlkIG1lZXQiPgoKPGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMC4wMDAwMDAsNzAwLjAwMDAwMCkgc2NhbGUoMC4xMDAwMDAsLTAuMTAwMDAwKSIKZmlsbD0iIzAwMDAwMCIgc3Ryb2tlPSJub25lIj4KPHBhdGggZD0iTTI0NzEgNDU2NCBjLTQ0MCAtNzYgLTcyNCAtMzQxIC04MjQgLTc2OCAtMjkgLTEyMyAtMzEgLTQ3NCAtNCAtNjA2CjE0IC03MiA1NCAtMjA0IDgyIC0yNzIgNyAtMTcgLTE1IC0xOCAtMzg0IC0xOCBsLTM5MSAwIDAgODI1IDAgODI1IC0yOTUgMAotMjk1IDAgMCAtMTA2NSAwIC0xMDY1IDcwMiAyIDcwMyAzIDUgMTk5IDUgMTk5IDM4IC01NiBjODcgLTEyNiAyNDMgLTI0NyAzOTEKLTMwMiAxNjQgLTYyIDIyMSAtNzAgNDgxIC03MCAxODEgMCAyNjkgNSAzNjAgMTkgMTEzIDE3IDM0NyA3MiA0MDggOTUgbDI3IDExCi0yIDU3NyAtMyA1NzggLTQ1NCAzIGMtMzI3IDEgLTQ1OCAtMSAtNDY3IC05IC0xOSAtMTUgLTIwIC00MjkgLTIgLTQ0NyA4IC04CjY1IC0xMiAxOTAgLTEyIGwxNzggMCAwIC0xNTQgMCAtMTUzIC0zOCAtNyBjLTM0OCAtNTUgLTU0NiA1MyAtNjI0IDM0MSAtMzAKMTEzIC0zMCAzNjMgMCA0NzYgNTUgMjAzIDE4MCAzMzAgMzYxIDM2OCAxMzkgMjkgNDE0IC04IDU2NSAtNzUgMzMgLTE1IDYzCi0yMyA2NyAtMTkgNCA1IDQ4IDEwOSA5OCAyMzMgNjUgMTYxIDg3IDIyNiA3OCAyMzIgLTI3IDE2IC0yMjkgNzggLTMyMiA5OAotMTMyIDI5IC01MDMgMzcgLTYzNCAxNHoiLz4KPHBhdGggZD0iTTUyNTMgNDU1MiBsLTczIC0zIDAgLTExMjMgMCAtMTEyNCAxMjMgLTYgYzUxMCAtMjcgNzI0IC0xIDk3MiAxMTkKMjM5IDExNiA0MTUgMzE4IDUwMyA1ODEgNTIgMTU0IDY2IDI1NiA2NiA0OTQgMSAyMDQgLTEgMjM0IC0yMyAzMTkgLTYyIDI0OAotMTc1IDQxNCAtMzcwIDU0NSAtMTQ5IDEwMSAtMzA3IDE1NiAtNTQ0IDE5MSAtMTAxIDE0IC00MDcgMTggLTY1NCA3eiBtNzk3Ci00OTIgYzE1MSAtNzYgMjEwIC0xODcgMjEwIC0zOTYgMCAtODcgMyAtMTE0IDEzIC0xMTQgMTkgMCA3NiAtNTkgODMgLTg0IDMKLTEyIDQgLTEyOSAyIC0yNjAgbC0zIC0yMzggLTM3IC0zNCAtMzggLTM0IC0zODEgMCAtMzgxIDAgLTMzIDMzIGMtNDMgNDMgLTQ3CjczIC00MyAzMjggMyAyMjUgNyAyNDAgNjkgMjc4IGwzMCAxOCAzIDEyNCBjNCAxNDkgMjQgMjEyIDkyIDI4OCA4MyA5MyAxNTkKMTIzIDI4OSAxMTcgNTggLTIgOTQgLTEwIDEyNSAtMjZ6Ii8+CjxwYXRoIGQ9Ik01ODQ3IDM4OTYgYy04OCAtMzIgLTExNiAtODkgLTExNyAtMjMzIGwwIC0xMDMgMTcwIDAgMTcwIDAgMCAxMDQKYzAgNTggLTUgMTE3IC0xMSAxMzMgLTE1IDQyIC02OCA5MiAtMTA5IDEwMyAtNDUgMTMgLTU3IDEyIC0xMDMgLTR6Ii8+CjxwYXRoIGQ9Ik01ODY0IDMzNTYgYy04MiAtMzYgLTk4IC0xMjYgLTMzIC0xODkgMTQgLTEzIDE5IC0zMiAxOSAtNzggbDAgLTYwCjUzIDMgNTIgMyAzIDU5IGMyIDQ2IDkgNjYgMjggODggMzYgNDQgMzQgMTEwIC01IDE0OSAtMTYgMTYgLTM2IDI5IC00NCAyOSAtOQowIC0yMSAyIC0yOSA1IC03IDIgLTI3IC0yIC00NCAtOXoiLz4KPHBhdGggZD0iTTM1NjAgMzQ4NiBsMCAtMTA2NiAyOTUgMCAyOTUgMCAwIDM0OSAwIDM0OCAxNzMgNiBjMjAyIDYgMjk2IDI2CjQyNiA4OSAxNTkgNzYgMjg3IDIzMyAzMzYgNDEzIDI2IDk0IDMyIDMyNCAxMCA0MjEgLTUzIDI0MyAtMjE1IDQwMyAtNDgwIDQ3NgotNzYgMjEgLTEwMiAyMiAtNTY3IDI2IGwtNDg4IDMgMCAtMTA2NXogbTgxNCA1NzQgYzU0IC0xMiA4NyAtMzggMTE0IC05MSAyMAotMzggMjMgLTU1IDE5IC0xMjEgLTUgLTk3IC0zOCAtMTU3IC0xMTEgLTIwMiAtNDMgLTI3IC01OSAtMzEgLTE0NyAtMzQgbC05OQotNCAwIDIzMSAwIDIzMSA5MyAwIGM1MCAwIDExMCAtNSAxMzEgLTEweiIvPgo8L2c+Cjwvc3ZnPgo=', 200);
	}

	function adminOptions(){

		if(isset($_POST['saveLgpd'])){
			if(!get_option('lgpd_cookieConsent')){
				add_option('lgpd_cookieConsent', $_POST['cookieConsent']);
				add_option('lgpd_cookieEssenciais', $_POST['cookieEssenciais']);
				add_option('lgpd_cookieOptionAnalise', $_POST['cookieOptionAnalise']);
				add_option('lgpd_cookieAcceptedScripts', $_POST['cookieAcceptedScripts']);
				add_option('lgpd_policy_link', $_POST['policy_link']);
				add_option('lgpd_policy_title', $_POST['policy_title']);
				add_option('lgpd_cor_fundo', $_POST['lgpd_cor_fundo']);
				add_option('lgpd_cor_text', $_POST['lgpd_cor_text']);
				add_option('lgpd_cor_botao', $_POST['lgpd_cor_botao']);
				add_option('lgpd_cor_botao_borda', $_POST['lgpd_cor_botao_borda']);
				add_option('lgpd_cor_botao_borda_hover', $_POST['lgpd_cor_botao_borda_hover']);
				add_option('lgpd_cor_botao_hover', $_POST['lgpd_cor_botao_hover']);
				add_option('lgpd_cor_texto_botao', $_POST['lgpd_cor_texto_botao']);
				add_option('lgpd_cor_texto_botao_hover', $_POST['lgpd_cor_texto_botao_hover']);
				add_option('lgpd_ativo', $_POST['lgpd_ativo']);
			}else {
				update_option( 'lgpd_cookieConsent', $_POST['cookieConsent'] );
				update_option( 'lgpd_cookieEssenciais', $_POST['cookieEssenciais'] );
				update_option( 'lgpd_cookieOptionAnalise', $_POST['cookieOptionAnalise'] );
				update_option( 'lgpd_cookieAcceptedScripts', $_POST['cookieAcceptedScripts'] );
				update_option('lgpd_policy_link', $_POST['policy_link']);
				update_option('lgpd_policy_title', $_POST['policy_title']);
				update_option('lgpd_cor_fundo', $_POST['lgpd_cor_fundo']);
				update_option('lgpd_cor_text', $_POST['lgpd_cor_text']);
				update_option('lgpd_cor_botao', $_POST['lgpd_cor_botao']);
				update_option('lgpd_cor_botao_borda', $_POST['lgpd_cor_botao_borda']);
				update_option('lgpd_cor_botao_borda_hover', $_POST['lgpd_cor_botao_borda_hover']);
				update_option('lgpd_cor_botao_hover', $_POST['lgpd_cor_botao_hover']);
				update_option('lgpd_cor_texto_botao', $_POST['lgpd_cor_texto_botao']);
				update_option('lgpd_cor_texto_botao_hover', $_POST['lgpd_cor_texto_botao_hover']);
				update_option('lgpd_ativo', $_POST['lgpd_ativo']);
			}
		}

		$cookieConsent = get_option('lgpd_cookieConsent', 'Usamos cookies para mejorar su experiencia de navegación. Para administrar sus preferencias, haga clic en configuración de cookies.');
		$cookieEssenciais = get_option('lgpd_cookieEssenciais', 'TEstos son estrictamente necesarios para el funcionamiento de la Plataforma Digital, sin ellos no podemos brindarle algunos servicios esenciales, como la funcionalidad básica de la Plataforma..');
		$cookieOptionAnalise = get_option('lgpd_cookieOptionAnalise', 'Al recopilar esta información, podemos comprender cómo se utilizan nuestras Plataformas y medir la efectividad de nuestras campañas de marketing, lo que ayuda a personalizar nuestros medios digitales. La desactivación de estas cookies puede resultar en daños en la entrega de algunas características de las Plataformas..');
		$cookieAcceptedScripts = stripslashes(get_option('lgpd_cookieAcceptedScripts', ''));
		$policyLink =  get_option('lgpd_policy_link', home_url( '/politica-de-privacidade' ));
		$policyTitle = get_option('lgpd_policy_title', 'Política de Privacidade');
		$corFundo = get_option('lgpd_cor_fundo', '#333333');
		$corTexto = get_option('lgpd_cor_text', '#FFFFFF');
		$corBotao = get_option('lgpd_cor_botao', '#333333');
		$corBotaoBorda = get_option('lgpd_cor_botao_borda', '#FFFFFF');
		$corBotaoBordaHover = get_option('lgpd_cor_botao_borda_hover', '#FFFFFF');
		$corBotaoHover = get_option('lgpd_cor_botao_hover', '#FFFFFF');
		$corTextoBotao = get_option('lgpd_cor_texto_botao', '#FFFFFF');
		$corTextoBotaoHover = get_option('lgpd_cor_texto_botao_hover', '#333333');
		$ativo = get_option('lgpd_ativo', 1);
		?>
        <h1>LGPD - Cookie</h1>
        <form method="post" class="sdlgpd">
            <div style="display: block; clear: both; padding-top: 20px;">
                <b>Habilitar tarja:</b>
                <label><input type="radio" <?php if($ativo)echo "checked";?> name="lgpd_ativo" value="1"> SIM</label>
                <label><input type="radio" <?php if(!$ativo)echo "checked";?> name="lgpd_ativo" value="0"> Não</label>
            </div>
            <div style="display: block; clear: both; padding: 20px 0;">
                <label>Botão da Política: <input type="text" name="policy_title" value="<?php echo $policyTitle; ?>" /> </label><br>
                <label>Link da Política: <input type="text" style="width: 500px;" name="policy_link" value="<?php echo $policyLink; ?>" /> </label><br>
            </div>
            <label>Texto sobre o uso de Cookies (mensagem inicial)<br>
                <textarea name="cookieConsent"><?php echo $cookieConsent; ?></textarea>
            </label>
            <br>
            <label>Texto sobre os Cookies essenciais<br>
                <textarea name="cookieEssenciais"><?php echo $cookieEssenciais; ?></textarea>
            </label>
            <br>
            <label>Texto sobre os Cookies Opcionais<br>
                <textarea name="cookieOptionAnalise"><?php echo $cookieOptionAnalise; ?></textarea>
            </label>
            <br>
            <label>JavaScripts para os Cookies Opcionais<br>
                <textarea name="cookieAcceptedScripts"><?php echo $cookieAcceptedScripts; ?></textarea>
            </label>
            <br>
            <div style="width: 30%; float: left;"><label>Cor de fundo
                    <input type="color" name="lgpd_cor_fundo" value="<?php echo $corFundo; ?>"></label></div>
            <div style="width: 30%; float: left;"><label>Cor do texto
                    <input type="color" name="lgpd_cor_text" value="<?php echo $corTexto; ?>"></label></div>
            <div style="width: 30%; float: left;"><label>Cor da borda do Botão
                    <input type="color" name="lgpd_cor_botao_borda" value="<?php echo $corBotaoBorda; ?>"></label></div>
            <div style="width: 30%; float: left;"><label>Cor do Botão
                    <input type="color" name="lgpd_cor_botao" value="<?php echo $corBotao; ?>"></label></div>
            <div style="width: 30%; float: left;"><label>Cor do texto do Botão
                    <input type="color" name="lgpd_cor_texto_botao" value="<?php echo $corTextoBotao; ?>"></label></div>
            <div style="width: 30%; float: left;"><label>Cor da borda do Botão MouseOver
                    <input type="color" name="lgpd_cor_botao_borda_hover" value="<?php echo $corBotaoBordaHover; ?>"></label></div>
            <div style="width: 30%; float: left;"><label>Cor do Botão MouseOver
                    <input type="color" name="lgpd_cor_botao_hover" value="<?php echo $corBotaoHover; ?>"></label></div>
            <div style="width: 30%; float: left;"><label>Cor do texto do Botão MouseOver
                    <input type="color" name="lgpd_cor_texto_botao_hover" value="<?php echo $corTextoBotaoHover; ?>"></label></div>
            <br>
            <div style="display: block; clear: both; padding-top: 20px;">
                <input style="background: #333;color: #FFF;padding: 7px 35px;border-radius: 3px;font-size: 15px;font-weight: bold;" type="submit" name="saveLgpd" value="Salvar">
            </div>
        </form>
        <style>
            .sdlgpd label {
                font-size: 15px;
                font-weight: bold;
            }
            .sdlgpd textarea {
                min-height: 80px;
                min-width: 60vw;
                margin: 10px 0 20px;
                font-size: 14px;
                font-weight: normal;
            }
        </style>
		<?php
	}

	function setSessionCheck(){
		if(!session_id())
			session_start();

		$retorno = '<!-- cookies aceitos -->
        ';
		if(isset($_POST['personalizar']) && $_POST['personalizar'] == '0') {
			$_SESSION['cookiePersonalizar'] = true;
		}else{
			$_SESSION['cookiePersonalizar'] = false;
			$retorno .= stripslashes(get_option('lgpd_cookieAcceptedScripts'));
		}
		$_SESSION['cookieConcent'] = true;

		exit(json_encode(array('success' => true, 'retorno' => $retorno)));
	}

	static function getSessionCheckPending(){
		if(!session_id())
			session_start();

		if(isset($_SESSION['cookieConcent']))
			return false;

		return true;
	}

	static function optionalIsDisalowed(){
		if(!session_id())
			session_start();

		if(isset($_SESSION['cookiePersonalizar']))
			return $_SESSION['cookiePersonalizar'];

		return true;
	}

	public static function cookieConsentFrontEnd(){
		$ativo = get_option('lgpd_ativo', 1);
		if(!$ativo)
			return;
		?>
        <script>jQuery(document).ready(function () {jQuery.ajax({type:"post",dataType: "html",url: '<?php echo home_url( '/wp-admin/admin-ajax.php' ); ?>',data: {action: "ajaxSetConsentFrontEnd"},success: function (response) {jQuery('body').append(response);}});});</script>
		<?php
	}

	public function ajaxSetFrontEnd(){
		if(CookieConsent::getSessionCheckPending()){
			$cookieConsent = get_option('lgpd_cookieConsent', 'Usamos cookies para mejorar su experiencia de navegación. Para administrar sus preferencias, haga clic en configuración de cookies.');
			$cookieEssenciais = get_option('lgpd_cookieEssenciais', 'Estos son estrictamente necesarios para el funcionamiento de la Plataforma Digital, sin ellos no podemos brindarle algunos servicios esenciales, como la funcionalidad básica de la Plataforma..');
			$cookieOptionAnalise = get_option('lgpd_cookieOptionAnalise', 'Al recopilar esta información, podemos comprender cómo se utilizan nuestras Plataformas y medir la efectividad de nuestras campañas de marketing, lo que ayuda a personalizar nuestros medios digitales. La desactivación de estas cookies puede resultar en daños en la entrega de algunas características de las Plataformas..');
			$policyLink =  get_option('lgpd_policy_link', home_url( '/politica-de-privacidade' ));
			$policyTitle = get_option('lgpd_policy_title', 'Política de Privacidade');
			$corFundo = get_option('lgpd_cor_fundo', '#333');
			$corTexto = get_option('lgpd_cor_text', '#FFF');
			$corBotao = get_option('lgpd_cor_botao', '#333');
			$corBotaoBorda = get_option('lgpd_cor_botao_borda', '#FFF');
			$corBotaoBordaHover = get_option('lgpd_cor_botao_borda_hover', '#FFF');
			$corBotaoHover = get_option('lgpd_cor_botao_hover', '#FFF');
			$corTextoBotao = get_option('lgpd_cor_texto_botao', '#FFF');
			$corTextoBotaoHover = get_option('lgpd_cor_texto_botao_hover', '#333');
			?>
            <div class="cookie-consent-wrapper">
                <div class="cookie-consent">
                    <div class="container mobile-privacy">
                        <div class="cookie-consent-text"><?php echo $cookieConsent; ?></div>
                        <div class="cookie-consent-accept">Aceptar todo</div>
                        <div class="cookie-consent-define cookie-config">Configurar cookies</div>
                        <a href='<?php echo $policyLink; ?>' class="btn-politica cookie-consent-privacy"><?php echo $policyTitle; ?></a>
                    </div>
                    <div class="cookie-personalizar">
                        <div class="cookie-item">
                            <div class="cookie-type">
                                <span>Cookies esenciales</span>
                                <input class="css-checkbox" type="checkbox" name="cookies_essenciais" value="1" checked disabled>
                                <label for="cookies_essenciais" class="css-label cb0"></label>
                                <div class="css-checkbox-trail"></div>
                            </div>
                            <div class="cookie-description">
								<?php echo $cookieEssenciais; ?>
                            </div>
                        </div>
                        <div class="cookie-item">
                            <div class="cookie-type">
                                <span>Cookies analíticas y publicitarias</span>
                                <input id="cookies-personalizar" class="css-checkbox" type="checkbox" name="cookies_marketing" checked value="1">
                                <label for="cookies_marketing" class="css-label cb0"></label>
                                <div class="css-checkbox-trail"></div>
                            </div>
                            <div class="cookie-description">
								<?php echo $cookieOptionAnalise; ?>
                            </div>
                        </div>
                    </div>
                </div>

                <script>
                    jQuery('.cookie-consent-accept').on("click", function () {
                        var personalizar = jQuery('#cookies-personalizar:checked').length;
                        jQuery.ajax({
                            type: "post",
                            dataType: "json",
                            url: '<?php echo home_url( '/wp-admin/admin-ajax.php' ); ?>',
                            data: {
                                action: "setCookieConsent",
                                personalizar: personalizar
                            },
                            success: function (response) {
                                if(response.success)
                                    jQuery('body').append(response.retorno);
                            }
                        });
                        jQuery('.cookie-consent-wrapper').remove();
                    });
                    jQuery('.cookie-config').on('click', function(){
                        jQuery('.cookie-personalizar').show();
                        jQuery('.cookie-consent-accept').html('Aceitar');
                        jQuery('.cookie-consent-accept').addClass('btn-accept');
                        jQuery('#cookies-personalizar').prop('checked',false);
                    });
                </script>

                <style>
                    .cookie-consent-wrapper {
                        position: fixed;
                        z-index: 1999;
                    }
                    .cookie-personalizar {
                        display: none;
                        position: absolute;
                        width: 400px;
                        bottom: 100%;
                        background-color: #e0e0e0;
                        left: 0;
                        max-width: 100%;
                        max-height: 75vh;
                        overflow-y: auto;
                    }
                    .cookie-type {
                        padding: 13px 20px 10px;
                        background-color: #333;
                        color: #FFF;
                        font-weight: 500;
                        text-align: left;
                        text-transform: uppercase;
                        font-size: 14px;
                    }
                    .cookie-description{
                        text-align: left;
                        padding: 20px;
                        border-bottom: 1px solid #999;
                        line-height: 24px;
                        font-size: 14px;
                        color: #333;
                    }
                    .cookie-consent-wrapper {
                        z-index:9999!important;
                    }
                    .cookie-consent{
                        position: fixed;
                        left: 0;
                        right: 0;
                        bottom: 0;
                        padding: 15px 0 10px;
                        background-color: <?php echo $corFundo; ?>;
                        color: <?php echo $corTexto; ?>;
                        text-align: center;
                        font-size: 15px;
                    }
                    .cookie-consent-text{margin-bottom: 30px;}
                    .cookie-consent-accept, .cookie-consent-define, .cookie-consent-privacy {
                        display: inline-block;
                        color: <?php echo $corTextoBotao; ?>;
                        border: 2px solid <?php echo $corBotaoBorda; ?>;
                        background-color: <?php echo $corBotao; ?>;
                        border-radius: 5px;
                        padding: 7px 25px;
                        float: right;
                        margin-right: 25px;
                        cursor: pointer;
                        -webkit-transition: background 1s ease-out;
                        -moz-transition: background 1s ease-out;
                        -o-transition: background 1s ease-out;
                        transition: background 1s ease-out;
                    }
                    .cookie-consent-accept:hover, .cookie-consent-define:hover, .cookie-consent-privacy:hover {
                        background-color: <?php echo $corBotaoHover; ?>;
                        color: <?php echo $corTextoBotaoHover; ?>;
                        border-color: <?php echo $corBotaoBordaHover; ?>;
                    }
                    input[type=checkbox].css-checkbox {
                        opacity: 0;
                        float: right;
                        width: 44px;
                        height: 24px;
                        z-index: 999;
                        position: relative;
                        cursor: pointer;
                    }
                    input[type=checkbox].css-checkbox + label.css-label.cb0 {
                        min-width: 29px;
                        height: 24px;
                        display: inline-block !important;
                        background-repeat: no-repeat;
                        background-position: 0 0;
                        cursor: pointer;
                        float: right;
                        margin: unset !important;
                        margin-right: -30px !important;
                    }
                    input[type=checkbox].css-checkbox:checked + label.css-label.cb0 {
                        background-position: 0 -24px;
                        margin-right: -49px !important;
                    }
                    input[type=checkbox].css-checkbox:disabled + label.css-label.cb0 {
                        filter: contrast(0.5);
                    }
                    label.css-label.cb0 {
                        background-image:url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAwCAYAAAALiLqjAAAGAElEQVRYha2WbWxT1xnHf+fca8fOWyEkzhDLFE2lE2FLae2UqiBNmzr6oqESqhZRrQvTRLTypV9aoVFcUSFlqjYErVomQofGmk7TKKPaq2jVgqpprWKHEJomXUdCFRVKyLsdx8719T374Nj4+tpxEvX5dl7u73+eR/ec5y8UxWO2dZvPEiIAIgCiBQgsLIVBhUCFpVLhynPv3irGEAUF2v16ZLw+COoAoC9yBgAT1OHq2rEOOnvMkgLTOx7dpElOK1RzCbAdhLiSsmhb9c4/L+fOy9xBZOfD+6RUoeXCARSqWUoViux8eJ9NOJPB9I5HN0mpQpQuSakwLUu0ZDJJZ9Du1zXJ6YJwt7s4SohCs2lWu1/PCkTG64P5ZREeL+UvvER119t49ux1wMqeeIqqrrcp3/8iwuO1rSlUc2S87gCAiLZu81lCu55/+vL9L6Lfd392bPzr7yTeOJ6G/2QPZa1PZtcmLlyAo7/C5bIhTKlS6/T0f55XGiHQmu+2Tbkf+XH6bIm4DQ6weutWLuy9yqamO3NFdEuIgExforxQCuOds45p9yPbHXCAoRMnmBmb4NKlfpLJ3KuQFmhxfAHMn/kj8+f+XGjJFsMnT/LJwYMARKOxPBHRIrl9/Z0iXb9fVGSos5PLzz0H6vZ1zRMJyKJfl4jJyUnMxsaCaxkRI5mUEggXg+T/Lbnw0dFRrIYGms6cKXgfotEYfb2DyPSrWAD+xFOLwjOhGhrY8OabBQ+oueQXEpQzAyFw73jcMf2/V1/l1scfO7evX4/0eh3zVeXlf5NSqTBgf2aVInWlzzb1+Suv8EkwyMDu3Yhr12xreiyGFY/bT69pNNbXnNI6PhuKzW+4Swe+n7vB7OkmUrWKsm+s5b9HjjBw+HB2bezsWeq2bEH6fOixGJ/t2kVqbs4mULO6untD76dH069pu1+Pjtf35L9HhmFwqXeA2WjMkT6A0HWU6egxeL0ew1dRWecfHo6kf9POHjNl0ZZfKrfbzb33NFFR4awvUBAupaS2bs3T/uHhCOQ0nPT7rZ4tJOL3f7eoiC0jKVi71tfV0j+YvZ1LbpmGYdDT008sFqdQeL0eo652TVvg08E/2USX0/QLiWiaxqo7KrsrpetHmbKUFliIQrZlft6QfZcHpcvtulpZ7vlHY33NqTUffHSjGGPR/rvlFw/gkgYKCUoACsCCB7oRKiSwLiUtt9n3wUdFGQUzCITbdTXhCyJYmi8SHBarb3WEA52lfdE97x7YJJGngeValysWVlvvto7ivihwPrhPIkMrgAM0S2QocD5Y2BctnPzr8UVYLZlMJKRrvlCWFcGlvR/oEnk6EG6/7YvUhC/ICsqiCUmwaQcf/uAgv9ywHSmyFW9WU760L2o+f8inC9Phi5YC7/jekzxYvzE798bVD/nttfcyQ9NU+jrpkobTF60ADvDD+iZGZxKZoe6SRkAqpMNVrPOu5qeNW2kor1kyHOBo//tMxQxuTMVRgEIGJMrui75TtZauzc/w7PqH6Nr8DBvv+OaS4PtD5zj1+X8AiMSTXJ+cw7LYLEHZMnih6TGqXemnuVL3cPzeNjZWrysJf23gom1uNmEyMhF70FH72WTCNq7UPbzu38Ng5Ab31Xx7SfBMJE1LlyBsruKlgb/wZXzStrFK9ywbDqBrYloi7L5oNBGhPXyK6/Gpoh8uBQ6gSTksBZbDF40mZtgb/h03EzMrhgNIVFgmLbfTFy2I/Dx0kqSVss2/NdS9NLgQlJVpr8u+hw7dQtFRaNPNxAx7uk8wZxoo4K8jV2j/91sl4QAVXv3iyM7f9AvFQoOZ9PVQ5D1KpRST0RQTeeaqWGhSmFqVqJtqPTYtAcKBTtPCcvii7AeaYFWVxKWVdvsCKHPJ56daj01DTsPp3dZxWSjh8EWZcGmSb9WWLyoiBdRWeY6P7T56LCu43JaZTFmMjM+RTFn5cKvS4/rZV7uO/MGW0Uqafq6IJgUVbv2imYzuHnu682b+3kV90d3nD/lc0ggoZCD9KGbeLRGOG+bVydl5vaLC9euh7S+PFGP8HzV0lfQrFJJLAAAAAElFTkSuQmCC");
                        -webkit-touch-callout: none;
                        -webkit-user-select: none;
                        -khtml-user-select: none;
                        -moz-user-select: none;
                        -ms-user-select: none;
                        user-select: none;
                        position: relative;
                        z-index: 2;
                    }
                    .css-checkbox-trail{
                        display: inline-block;
                        background-color: #ccc;
                        width: 45px;
                        height: 27px;
                        position: absolute;
                        right: 19px;
                        border-radius: 20px;
                        z-index: 0;
                        border: 1px solid #a9a9a9;
                        margin-top: -1px;
                    }
                    @media screen and (max-width: 600px){
                        .cookie-consent {
                            padding-bottom: 30px;
                        }
                        .mobile-privacy{
                            display: flex;
                            flex-direction: column;
                            gap: 10px;
                        }
                        .cookie-personalizar {
                            max-height: 55vh;
                            width: 100%;
                        }
                    }
                    @media screen and (max-width: 768px){
                        .cookie-consent {
                            font-size: 14px;
                            padding: 15px 5%;
                            max-width: 100%;
                            left: 0;
                            margin: 0;
                        }
                        .cookie-personalizar {
                            max-height: 50vh;
                        }
                        .cookie-consent-accept, .cookie-consent-define, .cookie-consent-privacy{
                            margin-right: 0;
                        }
                    }
                    @media screen and (max-width: 320px){
                        .cookie-personalizar{max-height: 52vh;}
                        .cookie-consent-text{margin-bottom:8px;}
                        .cookie-consent{font-size: 13px;}
                    }
                </style>
            </div>
			<?php
		}else{
			echo '<!-- Política aceita -->';
			if(!CookieConsent::optionalIsDisalowed())
				echo stripslashes(get_option('lgpd_cookieAcceptedScripts'));
		}
		exit();
	}

}
add_action( 'wp_footer', array('CookieConsent', 'cookieConsentFrontEnd'));
add_action( 'admin_menu', array('CookieConsent', 'menu_admin'));
add_action( 'wp_ajax_setCookieConsent', array('CookieConsent', 'setSessionCheck' ));
add_action( 'wp_ajax_nopriv_setCookieConsent', array('CookieConsent', 'setSessionCheck' ));
add_action( 'wp_ajax_ajaxSetConsentFrontEnd', array('CookieConsent', 'ajaxSetFrontEnd' ));
add_action( 'wp_ajax_nopriv_ajaxSetConsentFrontEnd', array('CookieConsent', 'ajaxSetFrontEnd' ));
