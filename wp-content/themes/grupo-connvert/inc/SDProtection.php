<?php

/**
 *  Proteção contra tentativa de liberar cadastros de admins
 */

if ( get_option( 'default_role' ) != 'subscriber'){
	if(get_option( 'default_role' ) == 'administrator')
		wp_mail('projetos@solucao.digital', 'Tentativa de invasão em '.home_url(), 'Ao carregar o functions.php o default_role estava como administrator. Já tiramos a brecha, mas é bom verificar....');
	update_option( 'default_role', 'subscriber' );
}

