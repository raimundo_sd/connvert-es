function regExTestValidDomain(url){
    var exp = new RegExp("^(http[s]?:\\/\\/(www\\.)?){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?");
    return exp.test(url);
}
function isYoutubeLink(url) {
    var exp = new RegExp(/(youtu\.be|youtube\.com)/);
    return exp.test(url);
}
function isVimeoLink(url) {
    var exp = new RegExp(/(vimeo\.com)/);
    return exp.test(url);
}
function YouTubeGetId(url){
    var ID = '';
    url = url.replace(/(>|<)/gi,'').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
    if(url[2] !== undefined) {
        ID = url[2].split(/[^0-9a-z_\-]/i);
        ID = ID[0];
    }
    else {
        ID = url;
    }
    return ID;
}
function YouTubeGetImage(id){
    return "http://img.youtube.com/vi/"+id+"/hqdefault.jpg";
}
function vimeoGetId(url){
    var regExp = /http:\/\/(www\.)?vimeo.com\/(\d+)($|\/)/;
    var match = url.match(regExp);
    if(match)
        return match[2];
    return false;
}
function vimeoGetImage(id){
    jQuery.ajax({
        type:'GET',
        url: 'http://vimeo.com/api/v2/video/' + id + '.json',
        jsonp: 'callback',
        dataType: 'jsonp',
        async: false,
        success: function(data){
            return data[0].thumbnail_large;
        }
    });
}