jQuery(document).ready(function($){
    'use strict';

    //mobile scroll handler to avoid lighthouse warning
    jQuery.event.special.touchstart = {
        setup: function( _, ns, handle ) {
            this.addEventListener("touchstart", handle, { passive: !ns.includes("noPreventDefault") });
        }
    };

    //Lazyload de CSS se colocado como media="none"
    jQuery('link[media=none]').attr('media','all');

    // Sub-menu Cases
    jQuery('.content-single-case .content-text h2, .content-single-case .content-text h3').each(function(key, element){
        if(jQuery(element).attr('id') !== undefined){
            jQuery('.breadcrumbs ul').append('<li><a href="#'+jQuery(element).attr('id')+'" class="link_scroll">'+jQuery(element).html()+'</a></li>');
        }

    });

    //ANCORA
    $(".link_scroll, .link_scroll a, #breadcrumbs a").click(function(){
        $('html,body').animate({scrollTop:$(this.hash).offset().top - 70}, 700);
        return false;
    });

    //onLoad scroll smoth
    if (location.hash){
        setTimeout(function(){
            jQuery('html, body').animate({scrollTop : (jQuery(location.hash).offset().top - 90)},700);
        }, 1000);
    }

    //CUSTOM UPLOAD
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).parent().parent().find(".custom-file-label").addClass("selected").html(fileName);
    });

    //MENU FIXO
    $(window).on('scroll', function () {
        if ($(window).scrollTop() > 50) {
            $('header').addClass('fixed');
        }else{
            $('header').removeClass('fixed');
        }
    });

    //OPEN MENU MOBILE
    $(".open-menu").click(function(){
        $(".full-menu").addClass("show-menu");
        $("body").addClass("show-menu");
        return false;
    });

    //CLOSE MENU MOBILE
    $(".close-menu, .overlay-menu").click(function(){
        $(".full-menu").removeClass("show-menu");
        $("body").removeClass("show-menu");
        return false;
    });

    $(".full-menu .bar-menu ul li a").click(function(){
        $(".full-menu").removeClass("show-menu");
        $("body").removeClass("show-menu");
    });

    $(".full-menu .bar-menu ul li.menu-item-has-children").append("<i class='fa fa-angle-down'></i>");

    $(".full-menu .bar-menu ul li.menu-item-has-children i").click(function(){
        $(this).prev("ul").slideToggle();
        $(this).toggleClass("fa-angle-down");
        $(this).toggleClass("fa-angle-up");
    });

    // slide posts
    if(jQuery(".carousel-conteudo").length > 0) {
        jQuery(".carousel-conteudo").slick({
            infinite: true, arrows: true, dots: false, slidesToShow: 3, responsive: [
                {breakpoint: 770, settings: {slidesToShow: 2, slidesToScroll: 2}},
                {breakpoint: 577, settings: {slidesToShow: 1, slidesToScroll: 1}}
            ]
        });
    }


    // slide depoimentos
    if(jQuery("#slide-depoimento > div > .wpb_wrapper").length > 0) {
        jQuery("#slide-depoimento   > div > .wpb_wrapper").slick({
            infinite: true, arrows: true, dots: false, slidesToShow: 1
        });
    }

    // slide home
    if(jQuery(".slider-home").length > 0) {
        jQuery(".slider-home").slick({
            arrows: false, autoplay: true, autoplaySpeed: 6000, dots: true, slidesToShow: 1
        });
    }

    function validateEmail(email) {
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    // Accordion ESG
    if(jQuery(".sec-content-socioambiental").length > 0){
        const accordionMenu = document.querySelectorAll('.sec-content-socioambiental .vc_tta-panel a');
        const accordionContent = document.querySelectorAll('.sec-content-socioambiental .vc_tta-panel .vc_tta-panel-body');
        if(accordionMenu.length && accordionContent.length){
            accordionMenu[0].classList.add('active');
            accordionContent[0].classList.add('active');

            function activeAccordion(index) {
                accordionContent[index].classList.toggle('active');
                accordionMenu[index].classList.toggle('active');
            }

            accordionMenu.forEach((itemMenu, index) => {
                itemMenu.addEventListener('click', () => {
                    activeAccordion(index);
                });
            });
        }
    }



    // Tabs - Informações Financeiras
    const tabMenu = document.querySelectorAll('.list-documentos .list-title li');
    const tabContent = document.querySelectorAll('.list-documentos .content-tabs');

    if(tabMenu.length && tabContent.length) {
        tabContent[0].classList.add('ativo');
        tabMenu[0].classList.add('ativo');

        function activeTab(index) {
            tabContent.forEach((table) => {
                table.classList.remove('ativo');
            });
            tabMenu.forEach((li) => {
                li.classList.remove('ativo');
            });
            tabContent[index].classList.add('ativo');
            tabMenu[index].classList.add('ativo');

        }

        tabMenu.forEach((itemMenu, index) => {
            itemMenu.addEventListener('click', () => {
                activeTab(index);
            });
        });
    }


    // Tabs - Materiais
    const tabMenu1 = document.querySelectorAll('.js-tabmenu li');
    const tabContent1 = document.querySelectorAll('.js-tabcontent section');

    if(tabMenu1.length && tabContent1.length) {
        tabContent1[0].classList.add('ativo');
        tabMenu1[0].classList.add('ativo');

        function activeTab(index) {
            tabContent1.forEach((section1) => {
                section1.classList.remove('ativo');
            });
            tabMenu1.forEach((li1) => {
                li1.classList.remove('ativo');
            });
            tabContent1[index].classList.add('ativo');
            tabMenu1[index].classList.add('ativo');
        }

        tabMenu1.forEach((itemMenu1, index) => {
            itemMenu1.addEventListener('click', () => {
                activeTab(index);
            });
        });
    }
});

function popUp(url) {
    window.open(url, "_blank", "width=480,height=430,menubar=no,toolbar=no,location=no");
}

// Button Scroll
function appendScrollToAnimation(section, destination, color = '#fff'){
    jQuery(section).append('<span id="mouseScroll" class="scroll-btn">' + '<a title="Rolar a página" href="' + destination + '"><span class="mouse"><span><i class="fas fa-chevron-down"></i></span></span></a></span>' +'<style>@-webkit-keyframes ani-mouse{0%{opacity:1;top:29%}15%{opacity:1;top:50%}50%{opacity:0;top:50%}100%{opacity:0;top:29%}}@-moz-keyframes ani-mouse{0%{opacity:1;top:29%}15%{opacity:1;top:50%}50%{opacity:0;top:50%}100%{opacity:0;top:29%}}@keyframes ani-mouse{0%{opacity:1;top:29%}15%{opacity:1;top:50%}50%{opacity:0;top:50%}100%{opacity:0;top:29%}}' + '.scroll-btn{display:none;position:fixed;text-align:center;bottom:5px;left:50%;margin-left: -17.5px;z-index:1000}.scroll-btn>*{display:inline-block;line-height:18px;font-size:13px;font-weight:400;color:#fff;color:' + color + ';font-family:FontAwesome;letter-spacing:2px}.scroll-btn>.active,.scroll-btn>:focus,.scroll-btn>:hover{color:' + color + '}.scroll-btn>.active,.scroll-btn>:active,.scroll-btn>:focus,.scroll-btn>:hover{opacity:.8}.scroll-btn .mouse{position:relative;display:block;width:40px;height:40px;margin:0 auto 60px;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;border:2px solid ' + color + ';border-radius:50%}.scroll-btn .mouse>*{position:absolute;display:block;top:29%;left: calc(50% - 10px);width:20px;height:20px;margin:-4px 0 0;border-radius:50%;font-size: 18px;-webkit-animation:ani-mouse 2.5s linear infinite;-moz-animation:ani-mouse 2.5s linear infinite;animation:ani-mouse 2.5s linear infinite}</style>');
    if(jQuery(section).height()+175 < jQuery(window).height()) {
        jQuery('#mouseScroll').css('position', 'absolute');
    }
    jQuery('#mouseScroll').fadeIn(1500);
    jQuery(window).on('scroll', function () {
        if(jQuery(window).scrollTop() > 50)
            jQuery('#mouseScroll').fadeOut(1500);
    });
    jQuery('#mouseScroll a').on('click', function (e) {
        e.preventDefault();
        jQuery('html,body').animate({scrollTop: jQuery(this.hash).offset().top - 180}, 300);
        return false;
    });
}
function afterReveal( el ) { el.addEventListener('animationend', function( event ) { jQuery(this).removeClass('wow'); jQuery('.wow').each(function(){ jQuery(this).css('opacity',1); }); }); }

// Função Scroll horizontal
function scrollX(btPrev, contentTab, btNext){
    const buttonNext = document.querySelector(btNext);
    const content = document.querySelector(contentTab);
    const buttonPrev = document.querySelector(btPrev);
    if(buttonNext){
        buttonNext.onclick = function () {
            content.scrollLeft += 50;
        };
    }

    if(buttonPrev){
        buttonPrev.onclick = function () {
            content.scrollLeft -= 50;
        };
    }
}



