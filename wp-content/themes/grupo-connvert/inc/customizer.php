<?php
/**
 * Funcional Bem Nutri Theme Customizer
 *
 * @package solucaodigital
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 */
function themeslug_sanitize_checkbox( $checked ) {
    // Boolean check.
    return ( ( isset( $checked ) && true == $checked ) ? true : false );
}

function solucaodigital_customize_register( $wp_customize ) {
    $wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
    $wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
    $wp_customize->get_setting( 'header_textcolor' )->transport = 'refresh';
    $wp_customize->get_control( 'header_textcolor'  )->section = 'site_name_text_color';
    $wp_customize->get_control( 'background_image'  )->section = 'site_name_text_color';
    $wp_customize->get_control( 'background_color'  )->section = 'site_name_text_color';

    // Add control for logo uploader
    $wp_customize->add_setting( 'project_logo', array(
        'sanitize_callback' => 'esc_url',
    ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'project_logo', array(
        'label'    => __( 'Upload Logo (replaces text)', 'solucaodigital' ),
        'section'  => 'title_tagline',
        'settings' => 'project_logo',
    ) ) );

    $wp_customize->add_setting( 'project_logo_2', array(
        'sanitize_callback' => 'esc_url',
    ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'project_logo_2', array(
        'label'    => __( 'Upload Logo 2 (replaces text)', 'solucaodigital' ),
        'section'  => 'title_tagline',
        'settings' => 'project_logo_2',
    ) ) );
    // texto footer
    $wp_customize->add_setting('text_footer');
    $wp_customize->add_control( new WP_Customize_Control($wp_customize, 'text_footer', array(
        'label' => 'Texto do Rodapé',
        'section'    => 'title_tagline',
        'settings'   => 'text_footer',
        'type' => 'textarea'
    ) ) );


    /*----------------- ADICIONA REDES SOCIAIS NO MENU PERSONALIZAR -----------*/
    // urls das midias sociais
    $wp_customize->add_section(
        'socialmedias',
        array(
            'title' => 'Redes Sociais',
            'priority' => 20,
        )
    );
    // facebook
    $wp_customize->add_setting( 'facebook_setting', array(
        'default' => 'https://www.facebook.com/',
        'sanitize_callback' => 'esc_url',
    ) );
    $wp_customize->add_control( new WP_Customize_Control($wp_customize, 'facebook_setting', array(
        'label' => 'Facebook',
        'section'    => 'socialmedias',
        'settings'   => 'facebook_setting',
        'type' => 'url'
    ) ) );
	// twitter
    $wp_customize->add_setting( 'twitter_setting', array(
        'default' => 'https://twitter.com/',
        'sanitize_callback' => 'esc_url',
    ) );
    $wp_customize->add_control( new WP_Customize_Control($wp_customize, 'twitter_setting', array(
        'label' => 'Twitter',
        'section'    => 'socialmedias',
        'settings'   => 'twitter_setting',
        'type' => 'url'
    ) ) );
	// instagram
	$wp_customize->add_setting( 'instagram_setting', array(
		'default' => 'https://www.instagram.com/',
		'sanitize_callback' => 'esc_url',
	) );
	$wp_customize->add_control( new WP_Customize_Control($wp_customize, 'instagram_setting', array(
		'label' => 'Instagram',
		'section'    => 'socialmedias',
		'settings'   => 'instagram_setting',
		'type' => 'url'
	) ) );
    // linkedin
    $wp_customize->add_setting( 'linkedin_setting', array(
        'default' => 'https://www.linkedin.com/',
        'sanitize_callback' => 'esc_url',
    ) );
    $wp_customize->add_control( new WP_Customize_Control($wp_customize, 'linkedin_setting', array(
        'label' => 'Linkedin',
        'section'    => 'socialmedias',
        'settings'   => 'linkedin_setting',
        'type' => 'url'
    ) ) );
    // whatsapp
    $wp_customize->add_setting( 'link_whatsapp_setting', array(
        'default' => 'https://www.whatsapp.com/',
        'sanitize_callback' => 'esc_url',
    ) );
    $wp_customize->add_control( new WP_Customize_Control($wp_customize, 'link_whatsapp_setting', array(
        'label' => 'Link Whatsapp',
        'section'    => 'socialmedias',
        'settings'   => 'link_whatsapp_setting',
        'type' => 'url'
    ) ) );

    // whatsapp
    $wp_customize->add_setting( 'whatsapp_setting', array(
        //'sanitize_callback' => 'esc_url',
    ) );
    $wp_customize->add_control( new WP_Customize_Control($wp_customize, 'whatsapp_setting', array(
        'label' => 'Whatsapp',
        'section'    => 'socialmedias',
        'settings'   => 'whatsapp_setting',
    ) ) );

    // youtube
    $wp_customize->add_setting( 'youtube_setting', array(
        'default' => 'https://www.youtube.com/',
        'sanitize_callback' => 'esc_url',
    ) );
    $wp_customize->add_control( new WP_Customize_Control($wp_customize, 'youtube_setting', array(
        'label' => 'Youtube',
        'section'    => 'socialmedias',
        'settings'   => 'youtube_setting',
        'type' => 'url'
    ) ) );
    /*----------------- ADICIONA INPUT DE CONTATOS NO MENU PERSONALIZAR -----------*/
    // email, telefone, endereço e horário de atendimento
    $wp_customize->add_section(
        'contatos',
        array(
            'title' => 'Contatos',
            'priority' => 21,
        )
    );
    // e-mail para Contato
    $wp_customize->add_setting( 'emailContato_setting', array(
        'default' => 'contato@solucao.digital',
        //'sanitize_callback' => 'esc_url',
    ) );
    $wp_customize->add_control( new WP_Customize_Control($wp_customize, 'emailContato_setting', array(
        'label' => 'E-mail',
        'section'    => 'contatos',
        'settings'   => 'emailContato_setting',
        'type' => 'email'
    ) ) );
    // telefone para contato
    $wp_customize->add_setting( 'telefoneContato_setting', array(
        'default' => '(48)3333-3333',
        //'sanitize_callback' => 'esc_url',
    ) );
    $wp_customize->add_control( new WP_Customize_Control($wp_customize, 'telefoneContato_setting', array(
        'label' => 'Telefone',
        'section'    => 'contatos',
        'settings'   => 'telefoneContato_setting',
        'type' => 'tel'
    ) ) );
    // telefone para contato - 2    
    $wp_customize->add_setting( 'telefoneContato2_setting', array(
        'default' => '(48)3333-3333',
        //'sanitize_callback' => 'esc_url',
    ) );
    $wp_customize->add_control( new WP_Customize_Control($wp_customize, 'telefoneContato2_setting', array(
        'label' => 'Telefone',
        'section'    => 'contatos',
        'settings'   => 'telefoneContato2_setting',
        'type' => 'tel'
    ) ) );
    // horário de atendimento
    $wp_customize->add_setting( 'horarioAtendimento_setting', array(
        'default' => 'Seg à Sex das 8h as 10h / Sab 8h as 12h',
        //'sanitize_callback' => 'esc_url',
    ) );
    $wp_customize->add_control( new WP_Customize_Control($wp_customize, 'horarioAtendimento_setting', array(
        'label' => 'Horário de atendimento',
        'section'    => 'contatos',
        'settings'   => 'horarioAtendimento_setting',
        'type' => 'text'
    ) ) );
    // Endereço
    $wp_customize->add_setting( 'endereco_setting', array(
        'default' => 'Rua Tenente Silveira, 123 - Centro - Florianópolis',
        //'sanitize_callback' => 'esc_url',
    ) );
    $wp_customize->add_control( new WP_Customize_Control($wp_customize, 'endereco_setting', array(
        'label' => 'Endereço',
        'section'    => 'contatos',
        'settings'   => 'endereco_setting',
        'type' => 'text'
    ) ) );
    // Cidade
    $wp_customize->add_setting( 'cidade_setting');
    $wp_customize->add_control( new WP_Customize_Control($wp_customize, 'cidade_setting', array(
        'label' => 'Bairro - Cidade',
        'section'    => 'contatos',
        'settings'   => 'cidade_setting',
        'type' => 'text'
    ) ) );
    //Cep
    $wp_customize->add_setting( 'cep_setting');
    $wp_customize->add_control( new WP_Customize_Control($wp_customize, 'cep_setting', array(
        'label' => 'CEP',
        'section'    => 'contatos',
        'settings'   => 'cep_setting',
        'type' => 'text'
    ) ) );

    // maps
    $wp_customize->add_setting( 'maps_setting', array(
        'default' => '',
        //'sanitize_callback' => 'esc_url',
    ) );
    $wp_customize->add_control( new WP_Customize_Control($wp_customize, 'maps_setting', array(
        'label' => 'Link Google Maps',
        'section'    => 'contatos',
        'settings'   => 'maps_setting',
    ) ) );

    // maps
    $wp_customize->add_setting( 'maps_setting', array(
        'default' => '',
        //'sanitize_callback' => 'esc_url',
    ) );
    $wp_customize->add_control( new WP_Customize_Control($wp_customize, 'maps_setting', array(
        'label' => 'Link Google Maps',
        'section'    => 'contatos',
        'settings'   => 'maps_setting',
    ) ) );

    // Politica de privacidade
    $wp_customize->add_setting( 'privacy_url_setting', array(
        'default' => '',
        //'sanitize_callback' => 'esc_url',
    ) );
    $wp_customize->add_control( new WP_Customize_Control($wp_customize, 'privacy_url_setting', array(
        'label' => 'Página Politica de Privacidade',
        'section'    => 'contatos',
        'settings'   => 'privacy_url_setting',
        'type' => 'dropdown-pages'
    ) ) );
    // Link site Covid
    $wp_customize->add_setting( 'link_url_site', array(
        'default' => '',
        //'sanitize_callback' => 'esc_url',
    ) );
    $wp_customize->add_control( new WP_Customize_Control($wp_customize, 'link_url_site', array(
        'label' => 'Site Covid',
        'section'    => 'contatos',
        'settings'   => 'link_url_site',
    ) ) );

}
add_action( 'customize_register', 'solucaodigital_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function solucaodigital_customize_preview_js() {
    wp_enqueue_script( 'solucaodigital_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'solucaodigital_customize_preview_js' );
