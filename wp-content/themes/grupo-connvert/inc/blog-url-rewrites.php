<?php
/**
 * Add new rewrite rule
 */
function create_new_url_querystring() {

	// blog archive
	add_rewrite_rule('^blog/?','index.php?post_type=post','top');

    // paginação do blog
    add_rewrite_rule('^blog/page/(\d+)/?','index.php?post_type=post&paged=$matches[1]','top');

    // single post - ok
    add_rewrite_rule('^blog/([^/]*)/?','index.php?name=$matches[1]','top');

    // categorias
    add_rewrite_rule('^blog/category/([^/]*)/?','index.php?post_type=post&category_name=$matches[1]','top');

    // paginação de categorias
    add_rewrite_rule('^blog/category/([^/]+)/page/(\d+)/?','index.php?category_name=$matches[1]&paged=$matches[2]','top');

    // tags
    add_rewrite_rule('blog/tags/([^/]+)/?','index.php?post_type=post&tag=$matches[1]','top');
}
add_action('init', 'create_new_url_querystring', 999 );

/**
 * Modify post link
 * This will print /estarpresente/blog/%category%/%post-name% instead of /post-name
 */
function append_query_string( $url, $post, $leavename ) {
    if ( !is_admin() && $post->post_type == 'post' ) {
        $url = home_url( user_trailingslashit( "blog/".$post->post_name ) );
    }
    return $url;
}
add_filter( 'post_link', 'append_query_string', 10, 3 );

function append_query_archive_string( $url, $post ) {
    if ( !is_admin() && $post->post_type == 'post' ) {
        if(is_array($post->post_category)) {
            $term     = get_term( $post->post_category[0] );
            $category = $term->slug."/";
        }else{
            $category = '';
        }
        $url = home_url( user_trailingslashit( "blog/category/".$category ) );
    }
    return $url;
}
add_filter( 'post_type_link', 'append_query_archive_string', 10, 2 );

// add the code below to your child theme's functions.php file.
function fwps_term_link_filter( $url, $term, $taxonomy ) {
    // change the industry to the name of your taxonomy
    if ( 'category' === $taxonomy ) {
        $url = home_url() . '/blog/category/' . $term->slug;
    }
    return $url;
}
add_filter( 'term_link', 'fwps_term_link_filter', 10, 3 );

/**
 * @param $query
 * @return mixed
 *
 * Ajusta a busca dentro do blog
 */
function serach_blog_rewrite_templates() {
    global $wp_query;
    if($wp_query->is_search && get_query_var('s') && $wp_query->query_vars['post_type'] == 'post'){
        add_filter('template_include', function () {
            return get_template_directory() . '/archive.php';
        });
    }elseif($wp_query->is_search){
        add_filter('template_include', function () {
            return get_template_directory() . '/search.php';
        });
    }
}
add_action( 'template_redirect', 'serach_blog_rewrite_templates' );

/** * Ajustando link no yoast post-sitemap.xml */
function sitemap_rewrite_url_for_post($url, $type, $post) {
    if ( $post->post_type == 'post' ) {
        $post_category = get_the_category($post->ID);
        if(is_array($post_category)) {
            $term     = get_term( $post_category[0] );
            $category = $term->slug."/";
        }else{
            $category = '';
        }
        $url['loc'] = home_url( user_trailingslashit( "/blog/".$post->post_name ) );
    }
    return $url;
}
add_filter('wpseo_sitemap_entry', 'sitemap_rewrite_url_for_post', 1, 3 );
add_filter('wpseo_enable_xml_sitemap_transient_caching', '__return_false');