<?php

class CustomFileUpload {

	public $metaValue;
	public $metaBoxTitle;
	public $post_type;
	public $metaboxPosition;
	public $extraFunction;

	public function __construct($metaValue, $metaBoxTitle, $post_type, $metaboxPosition = 'side', $extraFunction = false) {
		$this->metaValue = sanitize_title($metaValue);
		$this->metaBoxTitle = $metaBoxTitle;
		$this->post_type = $post_type;
		$this->metaboxPosition = $metaboxPosition;
		$this->extraFunction = $extraFunction;
		add_action( 'save_post', array($this, 'save_metaValue'), 1, 2); // save the custom fields
		add_action( 'add_meta_boxes', array($this, 'set_metabox' ));
	}

	public function set_metabox(){
		add_meta_box($this->post_type.'_'.$this->metaValue, $this->metaBoxTitle, array($this, 'the_metabox'), $this->post_type, $this->metaboxPosition, 'default');
	}

	public function save_metaValue($post_id, $post) {
		if ( !wp_verify_nonce( $_POST[$post->post_type.'_files_'.$this->metaValue.'_nonce'], plugin_basename(__FILE__) ))
			return $post->ID;

		if ( !current_user_can( 'edit_post', $post->ID ))
			return false;

		set_post_meta($post->ID, $this->metaValue);
	}

	public function the_metabox(){
		global $post;
		wp_enqueue_media();
		$file = get_post_meta($post->ID, $this->metaValue, true);
		echo '<input type="hidden" name="'.$post->post_type.'_files_'.$this->metaValue.'_nonce" value="'.wp_create_nonce(plugin_basename(__FILE__)) . '" />'; ?>
        <div>
            <div id="customFile_<?php echo $this->metaValue;?>" class="customFilePlaceHolder">
		        <?php if($file){
		            $fileUrl = wp_get_attachment_url( $file );
		            ?>
                    <li>
                        <a href="<?php echo $fileUrl; ?>" target="_blank"><i class="attachment-icon <?php echo substr($fileUrl, -3); ?>"></i> <?php echo get_the_title($file); ?></a>
                        <input type="hidden" name="<?php echo $this->metaValue; ?>" value="<?php echo $file; ?>"/>
                        <p id="edit_<?php echo $this->metaValue;?>" attachment="<?php echo $file; ?>" class="hide-if-no-js howto editAttachment">Click here to edit or update.</p>
                        <p id="del_<?php echo $this->metaValue;?>" attachment="<?php echo $file; ?>" class="pt-1 del-customFile">Remove attachment</p>
                    </li>
		        <?php }else{ ?>
                    <li><div id="add_<?php echo $this->metaValue;?>" class="add-customFile">Upload file</div></li>
		        <?php } ?>
            </div>
        </div>
        <style>
            .customFilePlaceHolder li {
                list-style: none;
                padding: 10px 0 5px;
            }
            .customFilePlaceHolder li a {
                display: block;
                text-align: center;
            }
            .add-customFile, .del-customFile {
                color: #0073aa;
                text-decoration: underline;
                font-size: 13px;
                line-height: 1.5;
                transition-property: border,background,color;
                transition-duration: .05s;
                transition-timing-function: ease-in-out;
                cursor: pointer;
            }
            .add-customFile:hover, .del-customFile:hover {
                color: #00a0d2;
            }
            .customFile .attachment-icon {
                content: '';
                background-color: #EEE;
                padding: 10px;
                display: block;
            }
            .edit-customFile {
                cursor: pointer;
            }
            .attachment-icon:before {
                font-family: 'Font Awesome 5 Free', sans-serif !important;
                font-weight: bold;
                font-size: 42px;
                font-style: normal;
                display: block;
                text-align: center;
            }
            .attachment-icon.pdf:before{
                content: '\f1c1';
            }
            .attachment-icon.doc:before{
                content: '\f1c2';
            }
            .attachment-icon.xls:before{
                content: '\f1c3';
            }
            .attachment-icon.ppt:before{
                content: '\f1c4';
            }
        </style>
        <script type='text/javascript'>
            jQuery(document).ready(function($){
                function add_customFile_<?php echo $this->metaValue;?>(button_class) {
                    var _custom_media = true,
                        _orig_send_attachment = wp.media.editor.send.attachment;
                    $('body').on('click', button_class, function(e) {
                        var button_id = '#'+$(this).attr('id');
                        var send_attachment_bkp = wp.media.editor.send.attachment;
                        var button = $(button_id);
                        _custom_media = true;
                        wp.media.editor.send.attachment = function(props, attachment){
                            if ( _custom_media ) {
                                $('#customFile_<?php echo $this->metaValue;?>').html('<li><a href="'+attachment.url+'" target="_blank"><i class="attachment-icon '+(attachment.filename.substr(attachment.filename.length - 3))+'"></i>'+attachment.filename+'</a><input type="hidden" name="<?php echo $this->metaValue;?>" value="'+attachment.id+'" /><p id="edit_<?php echo $this->metaValue;?>" attachment="'+attachment.id+'" class="hide-if-no-js howto editAttachment">Clicar aqui para editar ou atualizar.</p><div id="del_<?php echo $this->metaValue;?>" attachment="'+attachment.id+'" class="pt-1 del-customFile">Remover arquivo</div></li>');
                                del_customFile_<?php echo $this->metaValue;?>();
                                edit_customFile_<?php echo $this->metaValue;?>();
                            } else {
                                return _orig_send_attachment.apply( button_id, [props, attachment] );
                            }
                        }
                        wp.media.editor.open(button);
                        return false;
                    });
                }
                function edit_customFile_<?php echo $this->metaValue;?>(){
                    $('#edit_<?php echo $this->metaValue;?>').off().on('click', function () {
                        var button = $(this);

                        custom_uploader = wp.media.frames.file_frame = wp.media({
                            title: 'Editar imagem',
                            button: {
                                text: 'Confirmar'
                            },
                            multiple: false
                        });
                        custom_uploader.on('select', function() {
                            var attachment = custom_uploader.state().get('selection').first().toJSON();
                            $('#customFile_<?php echo $this->metaValue;?>').html('<li><a href="'+attachment.url+'" target="_blank"><i class="attachment-icon '+(attachment.filename.substr(attachment.filename.length - 3))+'"></i>'+attachment.filename+'</a><input type="hidden" name="<?php echo $this->metaValue;?>" value="'+attachment.id+'" /><p id="edit_<?php echo $this->metaValue;?>" attachment="'+attachment.id+'" class="hide-if-no-js howto editAttachment">Clicar aqui para editar ou atualizar.</p><div id="del_<?php echo $this->metaValue;?>" attachment="'+attachment.id+'" class="pt-1 del-customFile">Remover arquivo</div></li>');
                            del_customFile_<?php echo $this->metaValue;?>();
                            edit_customFile_<?php echo $this->metaValue;?>();
                        });
                        custom_uploader.on('open', function() {
                            var selection = custom_uploader.state().get( 'selection' );
                            selection.add( wp.media.attachment( $(button).attr('attachment') ) );
                        });
                        custom_uploader.open();
                        return false;
                    });
                }
                function del_customFile_<?php echo $this->metaValue;?>(){
                    $('#del_<?php echo $this->metaValue;?>').off().on('click', function () {
                        $('#customFile_<?php echo $this->metaValue;?>').html('<li><div id="add_<?php echo $this->metaValue;?>" class="add-customFile">Definir Arquivo</div></li>');
                        add_customFile_<?php echo $this->metaValue;?>('#add_<?php echo $this->metaValue;?>');
                    });
                }
                del_customFile_<?php echo $this->metaValue;?>();
                edit_customFile_<?php echo $this->metaValue;?>();
                add_customFile_<?php echo $this->metaValue;?>('#add_<?php echo $this->metaValue;?>');
            });
        </script>
<?php
        if($this->extraFunction){
            if(is_array($this->extraFunction)){
	            $reflection = new ReflectionMethod( $this->extraFunction[0], $this->extraFunction[1] );
            }else{
	            call_user_func($this->extraFunction);
            }
        }
	}

}