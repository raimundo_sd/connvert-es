<?php get_header(); ?>
<?php the_post(); ?>

    <section class="content-single-post subpages">
        <div class="container inner-post post-release">
            <div class="breadcrumbs" >
                <ul>
                    <li><a href="<?php echo home_url(); ?>">Home</a></li>
                    <li>|&nbsp; &nbsp;<a href="<?php echo home_url('/conteudo/'); ?>">Contents</a></li>
                    <li>|&nbsp; &nbsp;<?php the_title(); ?></li>
                </ul>
            </div>

           <h1 class="title-large"><?php the_title(); ?></h1>
            <div class="content-text">
                <?php the_content(); ?>
            </div>
        </div>
        <div class="container">
            <?php get_template_part('template-parts/share'); ?>
        </div>
    </section>

<?php get_footer(); ?>