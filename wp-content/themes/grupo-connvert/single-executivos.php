<?php get_header(); ?>
<?php the_post(); ?>

    <section class="content-single-post subpages">
        <div class="container">
            <h1 class="title-large" style="margin-bottom: 40px;"><?php the_title(); ?></h1>
        </div>
        <div class="container inner-post">
            <div id="breadcrumbs" >
                <ul>
                    <li><a href="/sobre#executivos">Ejecutivos</a></li>
                    <li>|&nbsp; &nbsp;<?php the_title(); ?></li>
                </ul>
            </div>
            <h1 class="title"><?php echo get_post_meta( get_the_ID(), 'cargo', true ); ?></h1>
            <div class="content-text">
                <?php the_content(); ?>
                <br>
                <a target="_blank" href="<?php echo get_post_meta( get_the_ID(), 'linkedin', true ); ?>" class="bt bt-vazado">Linkedin</a>
            </div>
        </div>
    </section>

<?php get_footer(); ?>