<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package solucaodigital
 */

?>
		<footer>
			<div class="top-footer">
				<div class="container">
					<div class="row">
						<div class="col-md-3">
							<a href="<?php echo site_url(); ?>" class="logo" title="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
								<img src="<?php echo get_theme_mod( 'project_logo' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
							</a>
							<div class="logo-empresas">
								<a rel="noreferrer" title="CXdzain" target="_blank" href="https://cxdzain.com.br/"><img src="<?php echo get_template_directory_uri(); ?>/images/logo-cxdzain.svg" alt=""></a>
								<a rel="noreferrer" title="Code7" target="_blank" href="https://code7.com/"><img src="<?php echo get_template_directory_uri(); ?>/images/logo-code7.svg" alt=""></a>
								<a rel="noreferrer" title="Flex Bpo" target="_blank" href="https://www.flexbpo.com.br/"><img src="<?php echo get_template_directory_uri(); ?>/images/logo-flex.svg" alt=""></a>
							</div>
						</div>

						<div class=" col-md-3  footer-menu">
							<?php wp_nav_menu(array("menu" => "primary", "container" => false)); ?>
						</div>

						<div class="col-md-5   box-address">
							<div class="contet-address">
								<?php if(get_theme_mod('endereco_setting') != ""): ?>
									<p><?php echo get_theme_mod('endereco_setting'); ?></p>
								<?php endif; ?>
								<?php if(get_theme_mod('cidade_setting') != ""): ?>
									<p class="title-address"><?php echo get_theme_mod('cidade_setting'); ?></p>
								<?php endif; ?>
								<?php if(get_theme_mod('cep_setting') != ""): ?>
									<p class="title-address">CEP: <?php echo get_theme_mod('cep_setting'); ?></p>
								<?php endif; ?>
							</div>
                            <div>
                                <?php if(get_theme_mod('link_url_site') != ""): ?>
                                    <a title="COVID-19: a nossa resposta" rel="noreferrer" target="_blank" href="<?php echo get_theme_mod( 'link_url_site', '' ); ?>">Covid-19: nuestra respuesta</a>
                                <?php endif; ?>
                            </div>
							<div class="info-contato">
								<p>Assessoria de Imprensa </p>	
								<?php if(get_theme_mod('telefoneContato_setting') != ""): ?>
									<a href="tel:+55<?php echo str_replace(array("(", ")", " ", "-", "+55"), "", get_theme_mod('telefoneContato_setting')); ?>" class="link-phone"><?php echo get_theme_mod('telefoneContato_setting'); ?></a> <br>
								<?php endif; ?>
								<?php if(get_theme_mod('telefoneContato2_setting') != ""): ?>
									<a href="tel:+55<?php echo str_replace(array("(", ")", " ", "-", "+55"), "", get_theme_mod('telefoneContato2_setting')); ?>" class="link-phone"><?php echo get_theme_mod('telefoneContato2_setting'); ?></a>
								<?php endif; ?>
							</div>
							
							<?php if(get_theme_mod('privacy_url_setting') != ""): ?>
									<a href="<?php echo get_permalink(get_theme_mod( 'privacy_url_setting', '' )); ?>">Política de privacidad y protección de datos</a>
							<?php endif; ?>
										
						</div>

                        <div class="col-md-1">
                            <ul class="redes-sociais">
                                <?php if(get_theme_mod('linkedin_setting') != ""): ?>
                                    <li><a title="Linkedin" rel="noreferrer" target="_blank" href="<?php echo get_theme_mod('linkedin_setting'); ?>"><i class="fab fa-linkedin"></i></a></li>
                                <?php endif; ?>
                                <?php if(get_theme_mod('instagram_setting') != ""): ?>
                                    <li><a title="Instagram" rel="noreferrer" target="_blank" href="<?php echo get_theme_mod('instagram_setting'); ?>"><i class="fab fa-instagram-square"></i></a></li>
                                <?php endif; ?>
                                <?php if(get_theme_mod('youtube_setting') != ""): ?>
                                    <li><a title="Youtube" rel="noreferrer" target="_blank" href="<?php echo get_theme_mod('youtube_setting'); ?>"><i class="fab fa-youtube-square"></i></a></li>
                                <?php endif; ?>
                                <?php if(get_theme_mod('facebook_setting') != ""): ?>
                                    <li><a title="Facebook" rel="noreferrer" target="_blank" href="<?php echo get_theme_mod('facebook_setting'); ?>"><i class="fab fa-facebook-square"></i></a></li>
                                <?php endif; ?>
                            </ul>
                        </div>
					</div>
				</div>
			</div>
			
			<div class="copy">
				<div class="container">
					<span>© <?php echo '  '.date('Y') . ' ' . bloginfo('name'); ?></span>
					<a rel="noreferrer" href="https://solucao.digital/" class="develop" title="Developed by: Solução Digital" target="_blank">
		                <img src="<?php bloginfo("template_url"); ?>/images/logo-sd-monocromatico.png" title="Developed by: Solução Digital" alt="Desenvolvido por: Solução Digital">
		            </a>
				</div>
			</div>
		</footer>

		<div class="full-menu">
		    <div class="overlay-menu"></div>
		    <div class="bar-menu">
		        <a title="Menu" href="#" class="close-menu"><i class="fa fa-times"></i></a>
		       <?php wp_nav_menu(array("menu" => "sitemap", "container" => false)); ?>
		    </div>
 		</div>

		<?php wp_footer(); ?>
	</body>
</html>
